<div class="col-lg-10 col-md-10 col-sm-10">
    <div class="col-lg-5 col-md-5 col-sm-5">
    <div class="form-group">
            <div class=="form-group">
                <h5><label for="nombre">First Name</label></h5>
                {!!Form::text('nombre', null, ['placeholder'=>'Nombre del Paciente', 'Autofocus', 'id' => 'nombre', 'class' => 'form-control'])!!}
                <div><h6><p class="text-danger"></p></h6></div>
                {!!Form::number('documento', null, ['placeholder'=>'Ingrese Nro Cedula', 'class' => 'form-control'])!!}
                <div><h6><p class="text-danger"></p></h6></div>
                <h5><em><p class="text-warning">No tiene Cédula</p></em></h5>
                Madre: {!!Form::radio('type', '1', false) !!} Padre: {!!Form::radio('type', '2', false) !!} Otro: {!!Form::radio('type', '3', false) !!}
            </div>
        </div>
    <div class="form-group">
            <h5><p class="text-primary">Fecha de Nacimiento</p></h5>
        {!!Form::text('fecha_nac', '',  ['class' => 'form-control datepicker col-md-3 col-xs-12'])!!}
        </div>
    </div>
    <div class="col-lg-5 col-md-5 col-sm-5">
    <div class="form-group">
            <h5><p class="text-primary">Sexo</p></h5>
            {!!Form::select ( 'sexo', array('masculino' => 'Masculino', 'femenino' => 'Femenino'), null, ['class' => 'form-control', ' style' => 'width:160px'])!!}</div>
    <div class="form-group">
            <h5><p class="text-primary">Estado</p></h5>
            {!! Form::select ('estados', $state_option, '',  ['id' => 'estado', 'class' => "form-control"])!!}
            <h5><p class="text-primary">Municipio</p></h5>
            {!! Form::select ('municipios', $muni_option, '',  ['id' => 'estado', 'class' => "form-control"])!!}
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <h5><p class="text-primary">Dirección y Teléfono</p></h5>
            {!!Form::number('telefono', null, ['placeholder'=>'Nº Telefono', 'class' => 'form-control', ' style' => 'width:160px'])!!}
        </div>
        <div class="form-group">
            {!!Form::textarea('direccion', null, ['placeholder'=>'Ingrese la direccion del paciente', 'class' => 'form-control', 'rows' => '3'])!!}
        </div>

        <div class="form-group">
            <h5><p class="text-primary">Diagnóstico</p></h5>
            {!!Form::textarea('diagnostico', null, ['placeholder'=>'Ingrese diagnostico del paciente', 'class' => 'form-control', 'rows' => '3'])!!}
        </div>
        <div class="form-group">
            <h5><p class="text-primary">Medico Tratante</p></h5>
            {!! Form::select ('medicos', $medic_option, '',  ['id' => 'estado', 'class' => "form-control"])!!}
        </div>
    </div>
</div>
<script>
    $('.datepicker').datepicker({
        format: "yyyy-mm-dd",
        language: "es",
        autoclose: true,
        orientation: "bottom auto"
    });
</script>