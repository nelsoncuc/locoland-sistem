@extends('layouts.master')
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid" >
            <div class="row">
                <div class="col-md-5">
                    <h2>Detalles del Paciente</h2>
                    <div class="list-group">
                        <li class="list-group-item list-group-item-info"><h4 class="text-capitalize">{{$datos->nombre}}</h4></li>
                        <li class="list-group-item">Cedula:  <strong>{{$datos->documento}}<br></strong></li>
                        <li class="list-group-item">Edad:  <?php function getAge($birthday) {
                                $birth = strtotime($birthday);
                                $now = strtotime('now');
                                $age = ($now - $birth) / 31536000;
                                return floor($age);
                            }
                            echo getAge($datos->fecha_nac) ." años";
                            ?><br>
                        <li class="list-group-item text-capitalize">Sexo: {{$datos->sexo}}</li>
                        <li class="list-group-item text-capitalize">Direccion: {{$datos->direccion}}</li>
                        <li class="list-group-item text-capitalize">Diagostico: {{$datos->historia->diagnostico}}</li>
                        <li class="list-group-item text-capitalize">Medico Tratante: </li>

                    </div>
                    <!-- {!! Html::link('person/edit/'.$datos->id, 'Editar', array('class' =>'btn btn-primary')); !!}-->
                    <a href="{{route('person.edit', $datos->id)}}">Editar</a>
                </div>
                <div class="col-md-5">
                    <h2>Historial de Entregas</h2>
                    <section class="box">
                        <table class="table table-bordered">
                            <thead>
                            <th scope="col">Paciente</th>
                            <th scope="col">fecha</th>
                            <th scope="col">Numero de Control</th>
                            </tr>
                            </thead>
                            @foreach($pip as $dato)
                                <tbody>
                                <tr>
                                    <td>{{$dato->persona_id}}</td>
                                    <td>{{$dato->fecha}}</td>
                                    <td>{{$dato->nro_control}}
                                        <button type="button" class="btn btn-primary btn-sm" data-container="body" data-toggle="popover" data-placement="bottom" data-original-title=""
                                                title="Medicamentos Entregados"
                                                data-content="{!!$dato->medicamento_id!!} -> {!!$dato->cantidad !!}"><div class="glyphicon glyphicon-plus"></div></button></td>
                                </tr>
                                </tbody>
                            @endforeach

                        </table>
                    </section>
                    <div class="row">
                        <div class="col-md-7">
                            {!! Html::link('person/', 'regresar', array('class' =>'btn btn-primary')); !!}

                            {!! Html::link('person/caja/'.$datos->id, 'Nueva Entrega', array('class' =>'btn btn-primary')); !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop


