@extends('layouts.master')
@section('content')
  <div style="margin-top:60px;">  
     <div id="tabla">
     <h1>Lista de Pacientes</h1>
     <section>
       <table class="table table-hover">
         <thead>
           <tr>
           <th scope="col">#</th>
             <th scope="col">Paciente</th>
             <th scope="col">Documento</th>
             <th scope="col">Ultima Entrega</th>
             <th scope="col">Nueva Entrega</th>
             <th scope="col">Registrar Med</th>
           </tr>
         </thead>
         
         @foreach($entregas as $entre)
         <tbody>
           <tr>
           <td>{{$entre->Persona->id}}</td>
             <td>{{$entre->Persona->nombre}}</td>
             <td>{{$entre->Persona->documento}}</td>
             <td>{{$entre->fecha}}</td>
             <td>{{$entre->prox_fecha}}</td>
             <td>{{HTML::link('medicine/add/'.$entre->Persona->id,'Registrar')}}</td>
           </tr>
         </tbody>
         @endforeach
  </table>
     </section>
     </div>

   </div>
@stop
