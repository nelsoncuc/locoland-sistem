@extends('layouts.master')

@section('content')
   <div class="container-fluid">
     <div class="col-xs-12 .col-md-10">
         <h3><p class="text-primary">Agregar Nuevos Pacientes</p></h3>
         {{ Form::open(array('url' => 'person/add', 'method' => 'post')) }}
                 @if(Session::has('mensaje'))
                 <div class="alert alert-success alert-dismissible" role="alert">
                     <p><h5>{{Session::get('mensaje')}}</h5></p>
                 </div>
                 @endif
            <div class="col-md-5">
                <div class="form-inline">
                    <div class=="form-group">
                        {{Form::text('nombre', null, array('placeholder'=>'Nombre del Paciente', 'Autofocus', 'class' => 'form-control'))}}
                        <div><h6><p class="text-danger"><em>{{$errors->first("nombre")}}</em></p></h6></div>
                        {{Form::number('documento', null, array('placeholder'=>'Ingrese Nro Cedula', 'class' => 'form-control'))}}
                        <div><h6><p class="text-danger"><em>{{$errors->first("documento")}}</em></p></h6></div>
                    </div>
                </div><br>
                <div class="form-group">
                    <label>Fecha de Nacimiento: </label><br>
                        {{ Form::date('fecha_nac') }}
                        {{$errors->first("nombre")}}
                </div>
                <div class="form-group">
                    <label>Sexo:</label><br>
                        {{Form::select ( 'sexo', array('1' => 'Masculino', '2' => 'Femenino'), null, array(  'class' => 'form-control', ' style' => 'width:160px') )}}</div>

                <div class="form-group">
                    <label>Estado:</label>
                        {{ Form::select ('estados', $state_option, Input::old('estados'), array ('id' => 'estado', 'class' => "form-control"))}}
                    <label>Municipio:</label>
                        {{Form::select('municipios',  $muni_option, Input::old('municipios'), array ('id' => 'municipio', 'class' => "form-control"))}}
                </div>
                    </div>
            <div class="col-md-5">

       <div class="form-group">
                  {{Form::textarea('direccion', null, array('placeholder'=>'Ingrese la direccion del paciente', 'class' => 'form-control', 'rows' => '3'))}}
                  </div>
                  <div class="form-group">
                  <label>Telefono y Dirección</label><br> 
                  {{Form::number('telefono', null, array('placeholder'=>'Nº Telefono', 'class' => 'form-control', ' style' => 'width:160px'))}}
                  </div>
                  <div class="form-group">
                  {{Form::textarea('diagnostico', null, array('placeholder'=>'Ingrese diagnostico del paciente', 'class' => 'form-control', 'rows' => '3'))}}
                  </div>
                  <div class="form-group">
                  <label>Medico tratante</label><br> 
                  {{ Form::select('medicos', $medic_option , Input::old('medicos'), array( 'class' => "form-control", ' style' => 'width:160px')) }}
                  </div>
            </div>
                 <div class="col-md-12">{{Form::submit('Registrar', array('class' => "btn btn-primary"))}}</div>
         {{ Form::close() }}
     </div>
   
@stop


</div>
@section('footer')

@stop