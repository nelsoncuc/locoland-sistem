@extends('layouts.master')
@section('content')
<div class="content-wrapper">
<div class="content-header">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Pacientes Registrados</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-bordered table-hover">
                        <thead>
    	                    <tr>
                            <th>#</th>
        	                <th>Nombre Paciente</th>
                            <th>Cedula</th>
                            <th>Historia</th>
                            <th>Telefono</th>
                            <th>ver</th>
                            </tr>
                        </thead>
                    @foreach($datos as $dato)
                        <tbody>
                            <tr>
                                <td>{{$dato->id}}</td>
                                <td>{{$dato->nombre}}</td>
                                <td>{{$dato->documento}}</td>
                                <td>00-{{$dato->historia->id}}</td>
                                <td>{{$dato->direccion}}</td>
                                <td>{!!Html::link('person/detail/'.$dato->id,'ver')!!}</td>
                                <td></td>
                            </tr>
                         </tbody>
                    @endforeach
                        </table>
                        {!!str_replace('/?', '?', $datos->render())!!}

                    </div><!-- /.box-body -->
                </div><!-- /.box -->
           </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
</div>
@stop

