@extends('layouts.master')
@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <section class="content">
                <div class="row">
            <section class="content col-md-11 col-sm-5">
                <div class="box box-primary">
                            <div class="box-header with-border">
                                <h2 class="text-light-blue">Editar Usuarios: {{$datos->nombre}}</h2>
                            <div class="box-tools pull-right">
                                 <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                            </div>
                    <div class="box-body">
                            {!! Form::model($datos, ['route' => 'person.update', $datos->id, 'method' => 'PUT'], 'csrf_token()') !!}

                            @if(Session::has('mensaje'))
                                <div class="alert alert-success alert-dismissible" role="alert">
                                    <p><h5>{!!Session::get('mensaje')!!}</h5></p>
                                </div>
                            @endif
                                <div class="row">
                                    @include('person.partials.fields')
                                    <div class="col-md-12">{!!Form::submit('Registrar', ['class' => "btn btn-primary"])!!}</div>
                                    {!! Form::close() !!}
                                </div>
                     </div>
                </div>
            </section>
        </div>
      </section>
     </div>
    </div>
@stop
