@extends('main ')

@section('content')


    <div class="col s12 m7">

        <div class="card horizontal">
            <div class="card-stacked">
                <div class="card-content">
                    <p>
                    <table>
                        <thead>
                        <tr>
                            <th data-field="id">Id</th>
                            <th data-field="name">Nombre Paciente</th>
                            <th data-field="price">Cedula</th>
                            <th data-field="price">Teléfono</th>
                            <th data-field="price">Ver</th>
                        </tr>
                        </thead>

                        @foreach($datos as $dato)
                            <tbody>
                            <tr>
                                <td>{{$dato->id}}</td>
                                <td>{{$dato->nombre}}</td>
                                <td>{{$dato->documento}}</td>
                                <td>{{$dato->direccion}}</td>
                                <td>{!!Html::link('person/detail/'.$dato->id,'ver')!!}</td>
                                <td></td>
                            </tr>
                            </tbody>
                        @endforeach
                    </table></p>
                </div>

            </div>
        </div>
    </div>

@stop


