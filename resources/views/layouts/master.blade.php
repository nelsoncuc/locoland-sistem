<!doctype html>
<html lang="es">
<html>
<head>
    <?php date_default_timezone_set('America/Caracas');?>
    @show
    <title>
    @section('titulo') 
    Sismema Saludm   
    @show
    </title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    {!!Html::style('css/app.css', array('media' => 'screen'))!!}
    {!!Html::style('css/bootstrap.min.css', array('media' => 'screen'))!!}
    {!!Html::style('css/AdminLTE.min.css', array('media' => 'screen'))!!}
    {!!Html::style('css/skins/_all-skins.min.css', array('media' => 'screen'))!!}
    {!!Html::style('fonts/ionicons.css', array('media' => 'screen'))!!}

</head>
<body class="skin-blue">
<div class="wrapper">
<div>
@section('header')
<header class="main-header">
    <a href="{{route('paciente.index')}}" class="logo"><b>Admin</b> Salud Mental</a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas"><span class="glyphicon glyphicon-menu-hamburger" ></span>
  </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

            </ul>
        </div>
    </nav>
</header>
@show
@section('nav')
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="header">MENU</li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dashboard"></i> <span>Pacientes</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('paciente.create')}}"><i class="fa fa-circle-o"></i> Registrar Paciente</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Buscar</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-files-o"></i>
                    <span>Medicamentos</span>

                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Buscar</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Registrar</a></li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Existencia</a></li>
                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-share"></i> <span>Multilevel</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                    <li>
                        <a href="#"><i class="fa fa-circle-o"></i> Level One <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                            <li><a href="#"><i class="fa fa-circle-o"></i> Level Two</a></li>
                            <li>
                                <a href="#"><i class="fa fa-circle-o"></i> Level Two <i class="fa fa-angle-left pull-right"></i></a>
                                <ul class="treeview-menu">
                                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                    <li><a href="#"><i class="fa fa-circle-o"></i> Level Three</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#"><i class="fa fa-circle-o"></i> Level One</a></li>
                </ul>
            </li>
            <li><a href="#"><i class="fa fa-book"></i> Ayuda</a></li>

        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
@show
    @yield('content')
</div>
</div>
    {!!Html::script('js/jquery.min.js')!!}
    {!!Html::script('js/bootstrap.min.js')!!}
    {!!Html::script('js/slimScroll/jquery.slimScroll.min.js')!!}
    {!!Html::script('js/fastclick/fastclick.min.js')!!}
    {!!Html::script('js/app.min.js')!!}


<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<!-- Jquery -->
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<!-- Datepicker Files -->
<link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-datepicker3.css')}}">
<link rel="stylesheet" href="{{asset('datePicker/css/bootstrap-standalone.css')}}">
<script src="{{asset('datePicker/js/bootstrap-datepicker.js')}}"></script>
<!-- Languaje -->
<script src="{{asset('datePicker/locales/bootstrap-datepicker.es.min.js')}}"></script>


    @section('footer')

    @show
@section('popover')
<script type="text/javascript">
$('[data-toggle="popover"]').popover();

$('body').on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});
</script>
@show
</body>
</html>
