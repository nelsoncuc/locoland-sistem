<div class="container">
@if (Session::has('errors'))
    <div class="alert alert-warning" style="width: 80%" role="alert">
        <ul>
            @foreach ($errors->all() as $error)
                <h5 class="text-center "><span class="glyphicon glyphicon-thumbs-up"></span> {{ $error }}</h5>
            @endforeach
        </ul>
    </div>
    @endif
</div>