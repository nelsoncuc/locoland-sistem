<head>
    <?php date_default_timezone_set('America/Caracas');?>
    @show
    <title>
        @section('titulo')
            Sismema Saludm
        @show
    </title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @include('pages.css')
</head>