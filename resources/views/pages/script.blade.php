{!!Html::script('assets/js/jquery-1.11.3.min.js')!!}
{!!Html::script('assets/js/bootstrap.min.js')!!}
{!!Html::script('assets/js/datepicker/moment.min.js')!!}
{!!Html::script('assets/js/datepicker/daterangepicker.js')!!}
{{--
{!!Html::script('assets/js/materialize.js')!!}
{!!Html::script('assets/js/materialize.min.js')!!}

{!!Html::script('assets/js/app.min.js')!!}
--}}
{!!Html::script('assets/js/lumino.glyphs.js')!!}
{!!Html::script('assets/js/bootstrap-table.js')!!}
{!!Html::script('assets/js/chartjs/Chart.min.js')!!}

<script src="//cdn.rawgit.com/noelboss/featherlight/1.7.2/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">

    $(function() {

        $('input[name="fecha_nac"]').daterangepicker({
                    singleDatePicker: true,
            orientation: "top",
                    showDropdowns: true,
                    dateFormat: "yy-mm-dd"
                });
    });

</script>

    <script type="text/javascript">

        $(function() {

            $('input[name="date_in"]').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                dateFormat: "yy-mm-dd"


            });
        });

</script>
<script type="text/javascript">

    $(function() {

        $('input[name="date_out"]').daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            dateFormat: "yy-mm-dd"

        });
    });

</script>

<script type="text/javascript">

    $(function() {

        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);

    });

</script>
<script type="text/javascript">
    $('[data-toggle="popover"]').popover();

    $('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
</script>

