<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar" style="margin: -42px 0 0 0">
    {!! Form::open(['route' => 'person/search']) !!}
    <div class="row">
        <div class="form-group col-lg-12 col-md-12 col-xs-12">
            {!!Form::text('parametro', null, array('placeholder'=>'Ingrese Busqueda', 'Autofocus', 'class' =>"form-control"))!!}
        </div>
    </div>
    {!! Form::close() !!}
    <ul class="nav menu">
        <li><a href="{{route('person.index')}}"><svg class="glyph stroked dashboard-dial"><use xlink:href="#stroked-dashboard-dial"></use></svg>Dashboard</a></li>


        <li class="parent">
            <a href="#">
                <span data-toggle="collapse" href="#sub-item-4"><svg class="glyph stroked chevron-down"><use xlink:href="#stroked-chevron-down"></use></svg>Pacientes</span>
            </a>
            <ul class="children collapse" id="sub-item-4">
                <li><a href="{{route('person.create')}}"><svg class="glyph stroked male user "><use xlink:href="#stroked-male-user"/></svg>Registrar</a></li>

            </ul>
        </li>
        <li class="parent ">
            <a href="#">
                <span data-toggle="collapse" href="#sub-item-1"><svg class="glyph stroked chevron-down"><use xlink:href="#stroked-chevron-down"></use></svg>Almacen</span>
            </a>
            <ul class="children collapse" id="sub-item-1">
                <li>
                    <a class="" href="{{route('warehouse/exist')}}">
                        <svg class="glyph stroked clipboard with paper"><use xlink:href="#stroked-clipboard-with-paper"/></svg>Existencia
                    </a>
                </li>
                <li>
                    <a class="" href="{{route('puchases/shop/index')}}">
                        <svg class="glyph stroked plus sign"><use xlink:href="#stroked-plus-sign"/></svg>Nueva Compra
                    </a>
                </li>
                <li>
                    <a class="" href="{{route('branchoffice/branchoffice')}}">
                        <svg class="glyph stroked upload"><use xlink:href="#stroked-upload"/></svg>Retiro Sucursal
                    </a>
                </li>
            </ul>
        </li>
        <li class="parent">
            <a href="#">
                <span data-toggle="collapse" href="#sub-item-2"><svg class="glyph stroked chevron-down"><use xlink:href="#stroked-chevron-down"></use></svg>Medicamentos</span>
            </a>
            <ul class="children collapse" id="sub-item-2">
                <li>
                    <a class="" href="{{route('warehouse.medicine.create')}}">
                        <svg class="glyph stroked plus sign"><use xlink:href="#stroked-plus-sign"/></svg>Nuevo
                    </a>
                </li>
                <li>
                    <a class="" href="#">
                        <svg class="glyph stroked plus sign"><use xlink:href="#stroked-plus-sign"/></svg>Editar
                    </a>
                </li>
                <li>
                    <a class="" href="{{route('warehouse/medicine/total')}}">
                        <svg class="glyph stroked heart"><use xlink:href="#stroked-heart"/></svg>Glosario
                    </a>
                </li>
            </ul>
        </li>
        <li class="parent">
            <a href="#">
                <span data-toggle="collapse" href="#sub-item-3"><svg class="glyph stroked chevron-down"><use xlink:href="#stroked-chevron-down"></use></svg>Reportes</span>
            </a>
            <ul class="children collapse" id="sub-item-3">
                <li>
                    <a class="" href="{{route('report/history_person')}}">
                        <svg class="glyph stroked plus sign"><use xlink:href="#stroked-plus-sign"/></svg>Por pacientes
                    </a>
                </li>
                <li>
                    <a class="" href="{{route('report/history_medi')}}">
                        <svg class="glyph stroked plus sign"><use xlink:href="#stroked-plus-sign"/></svg>Por Medicamentos
                    </a>
                </li>
                <li>
                    <a class="" href="{{route('report/medicamento_person')}}">
                        <svg class="glyph stroked plus sign"><use xlink:href="#stroked-plus-sign"/></svg>General Diagnostico
                    </a>
                </li>
            </ul>
        </li>

    </ul>
</div><!--/.sidebar-->
