@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="center-align" style="text-align: center">
                {{ Html::image('image/salud_mental-01.svg','Salud Mental Sucre',array('width' => 500, 'height' => 'auto')) }}
            </div>
            <div class="row" style="margin-bottom: 0; text-align: center; bottom: 0 !important">
                {{ Html::image('image/Logo-Min-Salud-2009.jpg','Salud Mental Sucre',array('width' => 400, 'height' => 'auto')) }}
            </div>
        </div>
    </div>
</div>
@endsection
