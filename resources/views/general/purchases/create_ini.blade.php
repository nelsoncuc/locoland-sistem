@extends('main')
@section('content')
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Registro de compras<p id="nombre"></p></div>
                            <div class="panel-body">

                                    {!! Form::open(['route'=>'puchases/shop/create', 'method' => 'get', 'class' => 'form-inline']) !!}

                                    <div class="form-group col-lg-5 col-md-5 col-xs-5">
                                        <label class="text-primary">N° de Referencia</label>
                                        {!!Form::text('codigo', null, array('placeholder'=>'Codigo de referencia', 'Autofocus', 'class' => 'form-control' ))!!}<div class="text-danger">{{$errors->first('codigo')}}</div>
                                    </div>
                                    <div class="form-group col-lg-6 col-md-6 col-xs-6">
                                        <label class="text-primary">Proveedor</label>
                                        {{ Form::select('provider', $provider, '',array( 'class' => 'form-control' )) }} {!! Html::link('/warehouse/provider/create', 'Registrar Nuevo') !!}<div class="text-danger">{{$errors->first('provider')}}</div>
                                    </div><br><br><br>
                                    <div class="row form-group" style="margin-left: 20px">
                                        {!!Form::submit('Continuar', array('class' =>'btn btn-info'))!!}
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div><!--/.row-->
            </div>
        </div>
    </div>

@stop