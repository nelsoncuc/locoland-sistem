@extends('main')
@section('content')
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">

                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }} <a href="javascript:void(0)" class="btn btn-large btn-warning text-center" onclick="window.open('{{route('/warehouse/medicine/add/create')}}','','width=700,height=600,noresize')">Regisrar Medicamento</a>
                        </div>
                    @endif
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Registro de compras</div>
                            <div class="panel-body">

                                    {!! Form::open(['route'=>'puchases/shop/continue', 'method' => 'post', 'class' => 'form-inline']) !!}
                                    {{csrf_field()}}
                                    <div class="row">
                                    <div class="form-group col-lg-4 col-md-4 col-xs-4">
                                        <label class="text-primary">N° de Referencia</label>
                                        {!!Form::text('codigo', Session::get('codigo'), array('placeholder'=>'Codigo de referencia', 'class' => 'form-control', 'readonly' => 'true' ))!!}<div class="text-danger">{{$errors->first('codigo')}}</div>
                                    </div>
                                    <div class="form-group col-lg-7 col-md-7 col-xs-7">

                                        {{ Form::hidden('provider', Session::get('provider'),array( 'class' => 'form-control', 'readonly' => 'true' )) }} <div class="text-danger">{{$errors->first('provider')}}</div>
                                    </div>
                                    </div><br>
                                    <div class="row">
                                        <div class="form-group col-sm-2">

                                            {{ Form::text('id_m', null,array('placeholder'=>'Codigo',  'class' => 'form-control', 'Autofocus')) }} <div class="text-danger">{{$errors->first('id_m')}}</div>
                                        </div>
                                        <div class="form-group col-sm-1 col-md-1 col-lg-2">

                                            {{ Form::text('cantidad', null,array('placeholder'=>'Cantidad',  'class' => 'form-control')) }} <div class="text-danger">{{$errors->first('cantidad')}}</div>
                                        </div>
                                        <div class="form-group col-sm-1 col-md-1 col-lg-2">

                                            {!!Form::text('date_out', '',  ['placeholder'=>'Fecha Venc.','class' => 'form-control datepicker col-md-3 col-xs-12'])!!} <div class="text-danger">{{$errors->first('date')}}</div>
                                        </div>
                                        <div class="form-group col-sm-1 col-md-1 col-lg-2">

                                            {{ Form::text('pu', null,array('placeholder'=>'Precio Unit.',  'class' => 'form-control', 'style' => 'width: 150px')) }} <div class="text-danger">{{$errors->first('pu')}}</div>
                                        </div>
                                        <div class="form-group col-sm-1 col-md-1 col-lg-2">

                                            {{ Form::text('total', null,array('placeholder'=>'Total',  'class' => 'form-control', 'style' => 'width: 150px')) }} <div class="text-danger" >{{$errors->first('total')}}</div>

                                        </div>
                                        {!!Form::submit('Ingresar', array('class' =>'btn btn-info'))!!}
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div><!--/.row-->
            </div>

            @if(isset($purchase2))
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Nueva Factura</div>
                            <div class="panel-body">
                                <table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="false" data-select-item-name="false" data-pagination="false" data-sort-name="false" data-sort-order="asc">
                                    <thead>
                                    <tr>
                                        <th data-field="medicamento" data-sortable="true" >Medicamnto</th>
                                        <th data-field="cantidad" data-sortable="true" >Cantidad</th>
                                        <th data-field="Vencimiento" data-sortable="false">Vencimiento</th>
                                        <th data-field="Precio" data-sortable="false">Precio U.</th>
                                        <th data-field="Total" data-sortable="false">Total</th>
                                        <th data-field="action" data-sortable="false">Accion</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($purchase2 as $facturas)
                                        <tr>
                                            <td>{{$facturas->nombre}}</td>
                                            <td>{{$facturas->cantidad}}</td>
                                            <td>{{$facturas->expires}}</td>
                                            <td>{{$facturas->pu}}</td>
                                            <td>{{$facturas->total}}</td>
                                            <td>{!!Html::link('puchases/shop/delete/'.$facturas->idp,'', array('class'=>'glyphicon glyphicon-remove', 'style'=>'color: red'))!!}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div style="float: right; padding-top: 10px">{!! Html::link('puchases/shop/add/'.Session::get('codigo'), 'Finalizar', array('class' =>'btn btn-info')) !!}</div>
                            </div>

                            </div>
                    </div>
                 </div><!--/.row-->
            </div>
            @endif

        </div>
    </div>
@stop