@extends('main')
@section('content')
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            <div class="container">
                @if (Session::has('errors'))
                    <div class="alert alert-warning" style="width: 80%" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <h5 class="text-center "><span class="glyphicon glyphicon-thumbs-up"></span> {{ $error }}</h5>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Ultimas facturas registradas</div>
                            <div class="panel-body">
                                <table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                                    <thead>
                                    <tr>
                                        <th data-field="nombre" data-sortable="true" >Nro de control</th>
                                        <th data-field="provider" data-sortable="true" >Proveedor</th>
                                        <th data-field="fecha ultima" data-sortable="false">Fecha</th>
                                        <th data-field="action" data-sortable="false">Acción</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($purchase as $facturas)
                                        <tr>
                                            <td>{{$facturas->control}}</td>
                                            <td>{{$facturas->name_provider}}</td>
                                            <td>{{substr($facturas->date,0,10)}}</td>
                                            <td data-featherlight data-featherlight-filter="a" data-featherlight-type="ajax">
                                                <a class="glyphicon glyphicon-eye-open" style="color: orangered" href="{{$url}}/purchases/getPurchasesPdf/{{$facturas->control}}" data-featherlight="iframe" data-featherlight-iframe-allowfullscreen="true" data-featherlight-iframe-width="1000" data-featherlight-iframe-height="600"></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! Html::link('/puchases/shop/create_ini', 'Nueva Factura', array('class' =>'btn btn-success')) !!}
                                <a href="javascript:history.back()" class="btn btn-info">Atras</a>
                            </div>
                        </div>
                    </div>
                </div><!--/.row-->
            </div>
        </div>
    </div>
@stop