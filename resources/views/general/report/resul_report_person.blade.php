@extends('main')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Nueva Busqueda por Pacientes</div>
                            <div class="panel-body">
                                <div>

                                    <div class="divider">
                                    </div>
                                    <div style="padding-left: 16px">Seleccione un rango de fechas a consultar</div><br>
                                </div>
                                <div  class="" style="margin-left: 10px">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="">
                                                {!! Form::open(['route'=>'report/entregas_p', 'method' => 'POST', 'class' => 'form-inline']) !!}

                                                {!!Form::text('date_in', '',  ['class' => 'form-control datepicker col-md-3 col-xs-12'])!!}
                                                {!!Form::text('date_out', '',  ['class' => 'form-control datepicker col-md-3 col-xs-12'])!!}

                                                {!!Form::submit('Consultar', array('class' =>'btn btn-success'))!!}

                                                {!! Form::close() !!}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Resultados  {!!Html::link('export/getPersonList/'.$date_in.'/2/'.$next_in,'Exportar (xls)', array('class' => 'btn btn-danger', 'style' => 'text-align: right'))!!} </div>
                            <div class="panel-body">
                                <table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                                    <thead>
                                    <tr>
                                        <th data-field="nombre_c" data-sortable="true" >Nombre Paciente</th>
                                        <th data-field="principio_a" data-sortable="true">Documento</th>
                                        <th data-field="presentacion"  data-sortable="true">Medicamento</th>
                                        <th data-field="cantidad" data-sortable="false">Cantidad</th>
                                        <th data-field="Farmaceutica" data-sortable="false">Fecha de entrega</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($entregas as $m)
                                        <tr>
                                            <td>{{$m->nombre}}</td>
                                            <td>{{$m->documento}}</td>
                                            <td>{{$m->nombre_c}} {{$m->composicion}}</td>
                                            <td>{{$m->cantidad}}</td>
                                            <td>{{$m->fecha}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!--/.row-->
            </div>
        </div>
    </div>
    <script>
        $(function () {
            $('[data-toggle="popover"]').popover()
        })
    </script>
@stop