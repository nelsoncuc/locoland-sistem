@extends('main')
{!!Html::script('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.js')!!}
@section('content')
    <div class="row">
        <div class="col-sm-12 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Nueva Busqueda por Medicamentos</div>
                            <div class="panel-body">
                                <div>

                                    <div class="divider">
                                    </div>
                                    <div style="padding-left: 16px">Seleccione un rango de fechas a consultar</div><br>
                                </div>
                                <div  class="" style="margin-left: 10px">
                                    <div class="row">
                                        <div class="col-lg-8">
                                            <div class="">
                                                {!! Form::open(['route'=>'report/entregas_m', 'method' => 'POST', 'class' => 'form-inline']) !!}

                                                {!!Form::text('date_in', '',  ['class' => 'form-control datepicker col-md-3 col-xs-12'])!!}
                                                {!!Form::text('date_out', '',  ['class' => 'form-control datepicker col-md-3 col-xs-12'])!!}

                                                {!!Form::submit('Consultar', array('class' =>'btn btn-success'))!!}

                                                {!! Form::close() !!}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Resultados  {!!Html::link('export/getMList/'.$date_in.'/2/'.$next_in,'Exportar xls (Excel)', array('class' => 'btn btn-danger', 'style' => 'text-align: right'))!!} </div>
                            <div class="panel-body">
                                <table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                                    <thead>
                                    <tr>
                                        <th data-field="nombre_c" data-sortable="true" >Medicamentos</th>
                                        <th data-field="principio" data-sortable="true" >Principio Activo</th>
                                        <th data-field="cantidad" data-sortable="true">Cantidad</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($entregas as $m)
                                        <tr>
                                            <td>{!!Html::link('report/person_medi/'.$m->id_m, $m->nombre_c.' '.$m->composicion)!!}</td>
                                            <td>{{$m->principio}}</td>
                                            <td>{{$m->total}}</td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Gráfica Medicamentos</div>
                            <div class="panel-body">
                                <div class="canvas-wrapper">
                                    <canvas class="main-chart" id="bar-chart" height="200" width="600"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!--/.row-->
            </div>
        </div>
    </div>

    <script>

            var barChartData = {
                labels : [@foreach($entregas as $m)'{{$m->principio}}', @endforeach],

            datasets : [
                    {
                        fillColor : "rgba(48, 164, 255, 0.2)",
                        strokeColor : "rgba(48, 164, 255, 0.8)",
                        highlightFill : "rgba(48, 164, 255, 0.75)",
                        highlightStroke : "rgba(48, 164, 255, 1)",
                        data : [@foreach($entregas as $m)'{{$m->total}}', @endforeach]
                    }
                ]

            }

            window.onload = function(){
                var chart2 = document.getElementById("bar-chart").getContext("2d");
                window.myBar = new Chart(chart2).Bar(barChartData, {
                    responsive : true
                });
            };

        </script>

    <script>
        $(function () {
            $('[data-toggle="popover"]').popover()
        })
    </script>

@stop