<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">Resultados</div>
        <div class="panel-body">
            <table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                <thead>
                <tr>
                    <th data-field="nombre_c" data-sortable="true" >Nombre Paciente</th>
                    <th data-field="principio_a" data-sortable="true">Documento</th>
                    <th data-field="presentacion"  data-sortable="true">Medicamento</th>
                    <th data-field="cantidad" data-sortable="false">Cantidad</th>
                    <th data-field="Farmaceutica" data-sortable="false">Fecha de entrega</th>

                </tr>
                </thead>
                <tbody>
                @foreach($result as $m)
                    <tr>
                        <td>{{$m->nombre}}</td>
                        <td>{{$m->documento}}</td>
                        <td>{{$m->nombre_c}} {{$m->composicion}}</td>
                        <td>{{$m->cantidad}}</td>
                        <td>{{$m->fecha}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>