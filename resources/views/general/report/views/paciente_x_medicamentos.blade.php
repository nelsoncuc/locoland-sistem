<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">Resultados</div>
        <div class="panel-body">
            <table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                <thead>
                <tr>
                    <th data-field="nombre_c" data-sortable="true" >Medicamentos</th>
                    <th data-field="principio_a" data-sortable="true">Cantidad</th>
                    <th data-field="Medicamento" data-sortable="true">Medicamento</th>

                </tr>
                </thead>
                <tbody>
                @foreach($result as $m)
                    <tr>
                        <td>{{$m->nombre}}</td>
                        <td>{{$m->documento}}</td>
                        <td>{{$m->nombre_c}} {{$m->composicion}}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>