<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">Resultados</div>
        <div class="panel-body">

            <table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                <thead>
                <spam><p>habia una vez</p></spam>
                <tr>
                    <th data-field="nombre_c" data-sortable="true" >Medicamentos</th>
                    <th data-field="principio_a" data-sortable="true" >Principio Activo</th>
                    <th data-field="principio_a" data-sortable="true">Cantidad</th>

                </tr>
                </thead>
                <tbody>
                @foreach($result as $m)
                    <tr>
                        <td>{{$m->nombre_c}} {{$m->composicion}}</td>
                        <td>{{$m->principio_a}}</td>
                        <td>{{$m->total}}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Bar Chart</div>
                <div class="panel-body">
                    <div class="canvas-wrapper">
                        <canvas class="main-chart" id="bar-chart" height="200" width="600"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/.row-->
    <script>
        var randomScalingFactor = function(){ return Math.round(Math.random()*1000)};

        var barChartData = {
            labels : ["January","February","March","April","May","June","July"],
            datasets : [
                {
                    fillColor : "rgba(220,220,220,0.5)",
                    strokeColor : "rgba(220,220,220,0.8)",
                    highlightFill: "rgba(220,220,220,0.75)",
                    highlightStroke: "rgba(220,220,220,1)",
                    data : [44,22,77,randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                },
                {
                    fillColor : "rgba(48, 164, 255, 0.2)",
                    strokeColor : "rgba(48, 164, 255, 0.8)",
                    highlightFill : "rgba(48, 164, 255, 0.75)",
                    highlightStroke : "rgba(48, 164, 255, 1)",
                    data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                }
            ]

        }


        window.onload = function(){
            var chart2 = document.getElementById("bar-chart").getContext("2d");
            window.myBar = new Chart(chart2).Bar(barChartData, {
                responsive : true
            });
        };

    </script>

    <script type="text/javascript">
        !function ($) {
            $(document).on("click","ul.nav li.parent > a > span.icon", function(){
                $(this).find('em:first').toggleClass("glyphicon-minus");
            });
            $(".sidebar span.icon").find('em:first').addClass("glyphicon-plus");
        }(window.jQuery);

        $(window).on('resize', function () {
            if ($(window).width() > 768) $('#sidebar-collapse').collapse('show')
        })
        $(window).on('resize', function () {
            if ($(window).width() <= 767) $('#sidebar-collapse').collapse('hide')
        })
    </script>
</div>