<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">Resultados </div>
        <div class="panel-body">
            <table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                <thead>
                <tr>
                    <th data-field="nombre_c" data-sortable="true" >Nombre Paciente</th>
                    <th data-field="Documento" data-sortable="true">Documento</th>
                    <th data-field="Direccion" data-sortable="true">Direccion</th>
                    <th data-field="Telefono" data-sortable="true">Telefono</th>
                    <th data-field="Diagnotico" data-sortable="true">Diagnostico</th>
                    <th data-field="Medicamento"  data-sortable="true">Medicamento</th>

                </tr>
                </thead>
                <tbody>
                @foreach($entregas as $m)
                    <tr>
                        <td>{{$m['nombre']}}</td>
                        <td>{{$m['documento']}}</td>
                        <td>{{$m['direccion']}}</td>
                        <td>{{$m['telefono']}}</td
                        <td>{{$m['diagnostico']}}</td>
                        <td>{{$m['medicamentos']}} </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>