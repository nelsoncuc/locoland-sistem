@extends('main')
@section('content')
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">Historial de Entrega por Pacientes</div>
                    <div class="panel-body">
                        <div>

                            <div class="divider">
                            </div>
                            <div style="padding-left: 16px">Seleccione un rango de fechas a consultar</div><br>
                        </div>
                        <div  class="" style="margin-left: 10px">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="">
                                        {!! Form::open(['route'=>'report/entregas_p', 'method' => 'POST', 'class' => 'form-inline']) !!}

                                        {!!Form::text('date_in', '',  ['class' => 'form-control datepicker col-md-3 col-xs-12'])!!}
                                        {!!Form::text('date_out', '',  ['class' => 'form-control datepicker col-md-3 col-xs-12'])!!}

                                        {!!Form::submit('Consultar', array('class' =>'btn btn-success'))!!}

                                        {!! Form::close() !!}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>

@stop