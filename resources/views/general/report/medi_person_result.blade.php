@extends('main')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Lista de pacientes por Medicamento</div>
                            <div class="panel-body">
                                <div>

                                    <div class="divider">
                                    </div>
                                    <div style="padding-left: 16px">Lista todos los pacientes registrados de acuerdo al medicamento seleccionado</div><br>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                           <div class="panel-heading">Resultados  {!!Html::link('export/getPersonsMedicine/'.$id,'Exportar xls (Excel)', array('class' => 'btn btn-danger', 'style' => 'text-align: right'))!!} </div>
                                <div class="panel-body">
                                <table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                                    <thead>
                                    <tr>
                                        <th data-field="nombre_c" data-sortable="true" >Medicamentos</th>
                                        <th data-field="principio_a" data-sortable="true">Cantidad</th>
                                        <th data-field="Medicamento" data-sortable="true">Medicamento</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($medicine as $m)
                                        <tr>
                                            <td>{{$m->nombre}}</td>
                                            <td>{{$m->documento}}</td>
                                            <td>{{$m->nombre_c}} {{$m->composicion}}</td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!--/.row-->
            </div>
        </div>
    </div>
    <script>
        $(function () {
            $('[data-toggle="popover"]').popover()
        })
    </script>
@stop