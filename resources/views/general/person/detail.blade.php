@extends('main')
@section('content')
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            <div class="col-lg-12">
                <div>
                    <h4 class="text-light-blue" style="padding-left: 16px">Detalle de paciente</h4>
                    <div class="divider">

                    </div>
                </div>
                <div  class="" style="margin-left: 10px">
                    <div class="row">
                        @include('general.person.person_info')
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



