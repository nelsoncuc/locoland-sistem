<div class="col-md-5">
    <div class="list-group">
        <li class="list-group-item list-group-item-info"><h4 class="text-capitalize">{{$datos->nombre}}</h4></li>
        <li class="list-group-item">Cedula:  <strong>{{$datos->documento}}<br></strong></li>
        <li class="list-group-item">Edad:  <?php function getAge($birthday) {
                $birth = strtotime($birthday);
                $now = strtotime('now');
                $age = ($now - $birth) / 31536000;
                return floor($age);
            }
            echo getAge($datos->fecha_nac) ." años";
            ?><br>
        <li class="list-group-item text-capitalize">Sexo: @if($datos->sexo == 1)
                                                            Masculino
                                                          @else femenino
                                                          @endif  </li>
        <li class="list-group-item text-capitalize">Direccion: {{$datos->direccion}}</li>
        <li class="list-group-item text-capitalize">Diagostico: {{$datos->historia->diagnostico}}</li>
        <li class="list-group-item text-capitalize">Medico Tratante: {{$datos->nombrem}}</li>

    </div>
    <div class="row" style="padding-left: 16px">
        {!! Html::link('/paciente', 'regresar', array('class' =>'btn btn-primary')) !!}
        <a href="{{route('paciente.edit', $datos->id)}}" class="btn btn-primary">Editar</a>

        {!! Html::link('person/caja/'.$datos->id, 'Entrega', array('class' =>'btn btn-success')) !!}

    </div>


</div>
<div class="col-md-7">
    <section class="box">
        <table data-toggle="table" data-url="tables/data2.json" >
            <thead>
            <th scope="col">fecha Ent.</th>
            <th scope="col">fecha Prox.</th>
            <th scope="col">Medicamento</th>
            <th scope="col">cantidad</th>
            </tr>
            </thead>
                <tbody>
                @foreach($pip as $dato)
                <tr>
                    <td>{{$dato->fecha}}</td>
                    <td>{{$dato->prox_fecha}}</td>
                    <td><strong>{{$dato->nombre_c}} {{$dato->composicion}}</strong></td>
                    <td class="text-info center-align">{{$dato->cantidad}}</td>
                </tr>
                @endforeach
                </tbody>
        </table>
        {{$pip->links() }}
    </section>
</div>

