@extends('main')
@section('content')
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            <div class="col-lg-12">
                <div>
                    <h4 class="text-light-blue">Editar Usuarios {{$datos->nombre}}</h4>
                    <div class="divider">
                    </div>
                </div>
                <div class="box-body">
                    {!! Form::model($datos, ['route' => ['person.update', $datos->id], 'method' => 'PUT'], 'csrf_token()') !!}

                    @if(Session::has('mensaje'))
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <p><h5>{!!Session::get('mensaje')!!}</h5></p>
                        </div>
                    @endif
                    <div class="row">
                        @include('general.person.partials.fields')
                        <div class="col-md-12">{!!Form::submit('Registrar', ['class' => "btn btn-primary"])!!}</div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
