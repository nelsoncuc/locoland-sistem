        <div class="col-md-8">
            <div class="row">
                <div class="form-group col-lg-10 col-md-10 col-xs-12">
                    <h5><p class="text-primary">Proveedor</p></h5>
                    {!! Form::select('code',$provider ,null, ['class'=>'form-control'] ) !!}
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-5 col-md-5 col-xs-12">
                    <h5><p class="text-primary">Fecha de Ingreso</p></h5>
                    <input type="text" name="fecha_nac" class="form-control"/><div class="text-danger">{{$errors->first('fecha_nac')}}</div>
                </div>
                <div class="form-group col-lg-5 col-md-5 col-xs-12">
                    <h5><p class="text-primary">Numero de Control</p></h5>
                        {!!Form::text('control', null, ['Autofocus', 'class' => 'form-control '])!!}<div class="text-danger">{{$errors->first('contro')}}</div>
                    </div>
                </div>

                <div class="divider"></div>
            <div class="row">
                <div class="form-group col-lg-4 col-md-4 col-xs-12">
                    <label>Medicamento</label>
                    {!!Form::number('documento', null, ['class' => 'form-control'])!!}<div class="text-danger">{{$errors->first('documento')}}</div>

                </div>
                <div class="form-group col-lg-4 col-md-4 col-xs-12">
                    <label>Cantidad</label>
                    {!!Form::number('documento', null, ['class' => 'form-control'])!!}<div class="text-danger">{{$errors->first('documento')}}</div>

                </div>
                <div class="form-group col-lg-4 col-md-4 col-xs-12">
                    <label>Vencimiento</label>
                    {!!Form::number('documento', null, ['class' => 'form-control'])!!}<div class="text-danger">{{$errors->first('documento')}}</div>

                </div>
            </div>
            <div class="row form-group col-lg-9 col-md-9 col-xs-10">
                <h5><p class="text-primary">No tiene dedula</p></h5>
                <p>
                    <input name="group1" type="radio" id="madre" value="1" />
                    <label for="madre">Madre</label>
                </p>
                <p>
                    <input name="group1" type="radio" id="padre" value="2" />
                    <label for="padre">Padre</label>
                </p>
                <p>
                    <input name="group1" type="radio" id="otro" value="3" />
                    <label for="otro">Otro</label>
                </p>
            </div>

            <div class="form-group form-group col-lg-5 col-md-5 col-xs-6">
                <h5><p class="text-primary">Sexo</p></h5>
                {!!Form::select ( 'sexo', array('masculino' => 'Masculino', 'femenino' => 'Femenino'), null, ['class' => 'form-control', ' style' => 'width:160px'])!!}
            </div>



</div>