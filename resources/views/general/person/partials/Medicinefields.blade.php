        <div class="col-md-12">
            <div class="row">
                    <div class="form-group col-lg-9 col-md-9 col-xs-10">
                        <label class="text-primary">Codigo</label>
                        {!!Form::text('codigo', null, ['Autofocus', 'class' => 'form-control '])!!}<div class="text-danger">{{$errors->first('codigo')}}</div>
                    </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-5 col-md-5 col-xs-5">
                    <label class="text-primary">Nombre Comercial</label>
                    {!!Form::text('nombre_c', null, ['class' => 'form-control '])!!}<div class="text-danger">{{$errors->first('nombre_c')}}</div>
                </div>
                <div class="form-group col-lg-5 col-md-5 col-xs-5">
                    <label class="text-primary">Principio Activo</label>
                    {!!Form::text('principio_a', null, ['Autofocus', 'class' => 'form-control '])!!}<div class="text-danger">{{$errors->first('principio_a')}}</div>
            </div>
            </div>
            <div class="row">
                <div class="form-group form-group col-lg-5 col-md-5 col-xs-6">
                <label class="text-primary">Presentacion</label>
                {!!Form::select ( 'presentacion', array('Comprimidos recubiertos' => 'Comprimidos recubiertos', 'Capsulas' => 'Capsulas', 'Suspension' => 'Suspension', 'Ampollas' => 'Ampollas', 'Tabletas' => 'Tabletas', 'Grageas' => 'Grageas'), null, ['class' => 'form-control', ' style' => 'width:300px'])!!}
            </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-9 col-md-9 col-xs-10">
                    <label class="text-primary">Farmaceutica</label>
                    {!!Form::text('farmaceutica', null, [ 'class' => 'form-control '])!!}<div class="text-danger">{{$errors->first('farmaceutica')}}</div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-4 col-md-4 col-xs-4">
                    <label class="text-primary">Cantidad</label>
                    {!!Form::number('cantidad', null, ['class' => 'form-control'])!!}<div class="text-danger">{{$errors->first('cantidad')}}</div>
                </div>

                <div class="form-group col-lg-6 col-md-6 col-xs-6">
                    <label class="text-primary">Composicion</label>
                    {!!Form::text('composicion', null, [ 'class' => 'form-control', ' style' => 'width:200px'])!!}<div class="text-danger">{{$errors->first('composicion')}}</div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-9 col-md-9 col-xs-10">
                    <label class="text-primary">Informacion</label>
                    {!!Form::textarea('info', null, [ 'class' => 'form-control '])!!}<div class="text-danger">{{$errors->first('info')}}</div>
                </div>
            </div>

        </div>

