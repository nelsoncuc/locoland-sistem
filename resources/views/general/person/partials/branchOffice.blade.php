        <div class="col-md-12">
            <div class="row">
                <div class="form-group col-lg-5 col-md-5 col-xs-5">
                    <label class="text-primary">Nombre Establecimiento</label>
                    {!!Form::text('name', null, ['class' => 'form-control '])!!}<div class="text-danger">{{$errors->first('name')}}</div>
                </div>
                <div class="form-group col-lg-5 col-md-5 col-xs-5">
                    <label class="text-primary">Rif</label>
                    {!!Form::text('rif', null, ['class' => 'form-control '])!!}<div class="text-danger">{{$errors->first('principio_a')}}</div>
            </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-5 col-md-5 col-xs-5">
                    <label class="text-primary">Telefono</label>
                    {!!Form::text('phone', null, [ 'class' => 'form-control '])!!}<div class="text-danger">{{$errors->first('phone')}}</div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-5 col-md-5 col-xs-5">
                    <label class="text-primary">Email</label>
                    {!!Form::email('email', null, ['class' => 'form-control'])!!}<div class="text-danger">{{$errors->first('email')}}</div>
                </div>
            </div>
        </div>

