<div class="row">
        <div class="col-lg-8">

            <div class="">
                {!! Form::open(['route'=>'caja.store', 'method' => 'POST', 'class' => 'form-inline']) !!}
                {{csrf_field()}}
                {!!Form::hidden('person', $person->id, array( 'class' => 'form-control' ))!!}
                {!!Form::number('cantidad', null, array('placeholder'=>'Cantidad', 'Autofocus', 'class' => 'form-control' ))!!}
                {!!Form::number('codigo', null, array('placeholder'=>'Cod. Medicamento', 'class' => 'form-control' ))!!}

                {!!Form::submit('Agregar', array('class' =>'btn btn-success'))!!}
                {!! Form::close() !!}
            </div>
        </div>
    <br><br>
        <div class="col-lg-8">
            <div class="">
                <table class="table table-hover">
                    <thead class="text-center">
                    <tr>
                        <th scope="col"></th>
                        <th scope="col">Medicamento</th>
                        <th scope="col">Cantidad</th>
                        <th scope="col">Costo</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    @foreach($retiro as $retir)
                        <tbody class="text-center">
                        <tr>
                            <td ><span class="glyphicon glyphicon-chevron-right info" aria-hidden="true"></span>

                            </td>
                            <td>
                                {{$retir->medicamento->nombre_c}}
                            </td>
                            <td>
                                {{$retir->cantidad}}
                            </td>
                            <td>
                                0.00
                            </td>
                            <td>@include('general.caja.delete')</td>
                        </tr>
                        </tbody>
                    @endforeach
                </table>
            </div>
        </div>
</div>

