        <div class="col-md-6">
                <h5><p class="text-primary">Identificacion</p></h5>
            <div class="row">
                    <div class="form-group col-lg-9 col-md-9 col-xs-10">
                        <label>Nombre  y apellido</label>
                        {!!Form::text('nombre', null, ['Autofocus', 'class' => 'form-control '])!!}<div class="text-danger">{{$errors->first('nombre')}}</div>
                    </div>
                </div>
            <div class="row">
                <div class="form-group col-lg-9 col-md-9 col-xs-10">
                    <label>Numero de cedula</label>
                    {!!Form::number('documento', null, ['class' => 'form-control'])!!}<div class="text-danger">{{$errors->first('documento')}}</div>

                </div>
            </div>
            <div class="row form-group col-lg-9 col-md-9 col-xs-10">
                <h5><p class="text-primary">No tiene dedula</p></h5>
                <p>
                    <input name="group1" type="radio" id="madre" value="1" />
                    <label for="madre">Madre</label>
                </p>
                <p>
                    <input name="group1" type="radio" id="padre" value="2" />
                    <label for="padre">Padre</label>
                </p>
                <p>
                    <input name="group1" type="radio" id="otro" value="3" />
                    <label for="otro">Otro</label>
                </p>
            </div>
            <div class="form-group col-md-5 col-xs-12">
                <h5><p class="text-primary">Fecha de Nacimiento</p></h5>
                <input type="text" name="fecha_nac" class="form-control"/><div class="text-danger">{{$errors->first('fecha_nac')}}</div>
            </div>
            <div class="form-group form-group col-lg-5 col-md-5 col-xs-6">
                <h5><p class="text-primary">Sexo</p></h5>
                {!!Form::select ( 'sexo', array('masculino' => 'Masculino', 'femenino' => 'Femenino'), null, ['class' => 'form-control', ' style' => 'width:160px'])!!}
            </div>
            <div class="form-group col-md-10 col-xs-12">
                <h5><p class="text-primary">Estado</p></h5>
                {!! Form::select ('estados', $state_option, ('estados'),  ['id' => 'estado', 'class' => "form-control"])!!}<div class="text-danger">{{$errors->first('estados')}}</div>
                <h5><p class="text-primary">Municipio</p></h5>
                {!! Form::select ('municipios', $muni_option, ('municipios'),  ['id' => 'estado', 'class' => "form-control"])!!}<div class="text-danger">{{$errors->first('municipios')}}</div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group col-lg-10 col-md-10 col-xs-12">
            <label class="text-primary">Telefono de contacto</label>
            {!!Form::number('telefono', null,  ['class' => "form-control"])!!}
            </div>
            <div class="form-group col-lg-10 col-md-10 col-xs-12">
                    <label class="text-primary">Direccion</label>
                    {!!Form::textarea('direccion', null, ['class' => 'form-control'])!!}<div class="text-danger">{{$errors->first('direccion')}}</div>

                </div>
            <div class="form-group col-lg-10 col-md-10 col-xs-12">
                    <label class="text-primary">Diagnostico</label>
                    {!!Form::textarea('diagnostico', $datos_H->diagnostico, ['class' => 'form-control'])!!}<div class="text-danger">{{$errors->first('diagnostico')}}</div>
                </div>
        <div class="form-group col-lg-10 col-md-10 col-xs-12">
            <h5><p class="text-primary">Medico Tratante</p></h5>
            {{ Form::select ('medicos', $medic_option, ('medicos'), ['id' => 'estado', 'class' => "form-control"])}}<div class="text-danger">{{$errors->first('medicos')}}</div>
        </div>
    </div>
