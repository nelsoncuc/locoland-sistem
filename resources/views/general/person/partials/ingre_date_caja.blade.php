<div class="row">
            <div class="col-lg-8">
            <div class="">
                {!! Form::open(['route'=>'caja.create', 'method' => 'GET', 'class' => 'form-inline']) !!}

                {!!Form::text('date_in', $date,  ['class' => 'form-control datepicker col-md-3 col-xs-12'])!!} {!!Form::text('date_out', '',  ['class' => 'form-control datepicker col-md-3 col-xs-12'])!!}

                {!!Form::hidden('id', $person->id, array('class' => 'form-control'))!!}

                {!!Form::submit('Registrar', array('class' =>'btn btn-success'))!!}

                {!! Form::close() !!}
            </div><!-- /.box-body -->
        </div>

</div>
