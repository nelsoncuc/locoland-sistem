@extends('main')
@section('content')
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            @if(Session::has('message'))
                <div class="alert alert-success" role="alert">
                    {{Session::get('message')}}
                </div>
            @endif
            <div class="col-md-6">
                <div class="list-group">
                    <li class="list-group-item list-group-item-info"><h4 class="text-capitalize">{{$id->nombre}}</h4></li>
                    <li class="list-group-item">Cedula:  <strong>{{$id->documento}}<br></strong></li>
                    <li class="list-group-item">Edad:  <?php function getAge($birthday) {
                            $birth = strtotime($birthday);
                            $now = strtotime('now');
                            $age = ($now - $birth) / 31536000;
                            return floor($age);
                        }
                        echo getAge($id->fecha_nac) ." años";
                        ?><br>
                    <li class="list-group-item text-capitalize">Sexo: @if($id->sexo == 1)
                            Masculino
                        @else femenino
                        @endif  </li>
                    <li class="list-group-item text-capitalize">Direccion: {{$id->direccion}}</li>


                </div>
                <div class="row">
                    <div class="col-md-10">
                    {!! Html::link('/paciente', 'regresar', array('class' =>'btn btn-primary')) !!}
                    <a href="{{route('paciente.edit', $id->id)}}" class="btn btn-primary">Editar</a>

                    {!! Html::link('person/caja/'.$id->id, 'Nueva Entrega', array('class' =>'btn btn-success')) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
