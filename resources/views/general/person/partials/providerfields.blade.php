        <div class="col-md-12">
            <div class="row">
                    <div class="form-group col-lg-9 col-md-9 col-xs-10">
                        <label class="text-primary">Nombre Proveedor</label>
                        {!!Form::text('provider', null, ['Autofocus', 'class' => 'form-control '])!!}<div class="text-danger">{{$errors->first('provider')}}</div>
                    </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-5 col-md-5 col-xs-5">
                    <label class="text-primary">Rif</label>
                    {!!Form::text('rif', null, ['class' => 'form-control '])!!}<div class="text-danger">{{$errors->first('rif')}}</div>
                </div>
            </div>
        </div>

