@extends('main')
@section('content')

    <div class="row">

        <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            <div class="col-sm-10">
            <div class="panel panel-blue">
                <div class="panel-heading dark-overlay blue-grey text-info"><svg class="glyph stroked clipboard-with-paper"><use xlink:href="#stroked-clipboard-with-paper"></use></svg>Resultados</div>
                <div class="panel-body">
                    <ul class="todo-list">
                        @foreach($datos as $dato)
                        <li class="todo-list-item">
                            <div class="checkbox">

                                <label>{{$dato->id}}</label>
                                <label>{{$dato->nombre}}</label>
                                <label>{{$dato->documento}}</label>



                            </div>
                            <div class="pull-right action-buttons">
                                {{Html::link('person/detail/'.$dato->id,'ver')}}
                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>

            </div>
            </div>

        </div>
    </div>


@stop


