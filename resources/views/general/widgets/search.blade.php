    <div class="small-box  teal darken-1">
        <div class="inner">
            @if(Session::has('mensaje'))
                <h2>{{Session::get('mensaje')}}</h2>
            @endif

                {!! Form::open(['route' => 'person/search']) !!}
                <div class="row">
                    <div class=" col s7">
                        {!!Form::text('parametro', null, array('placeholder'=>'Ingrese Busqueda', 'Autofocus', 'class' =>"validate"))!!}
                    </div>
                    <div class="input-field col s4">
                        {!!Form::submit('Buscar', array('class' => "waves-effect waves-light btn"))!!}
                    </div>
                </div>
                {!! Form::close() !!}
        </div>
        <a href="#" class="small-box-footer">Buscar Paciente</a>
    </div>
