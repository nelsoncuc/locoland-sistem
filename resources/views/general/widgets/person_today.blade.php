
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Pacientes para el dia de hoy</div>
                <div class="panel-body">
                    <table data-toggle="table" data-url="tables/data2.json" >
                        <thead>
                        <tr>
                            <th data-field="nombre" data-align="left">Nombre Paciente</th>
                            <th data-field="cedula">Cedula</th>
                            <th data-field="detalle">Detalle</th>

                        </tr>
                        </thead>

                            <tbody>
                            @foreach($entregas as $ent)
                            <tr>
                                <td>{{$ent->userEntrega->nombre}}</td>
                                <td>{{$ent->userEntrega->documento}}</td>
                                <td>{!!Html::link('person/detail/'.$ent->persona_id,'ver')!!}</td>
                            </tr>
                            @endforeach
                            </tbody>

                    </table>
                </div>
            </div>
        </div>
