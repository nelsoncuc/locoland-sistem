@extends('main')
@section('content')
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            <div class="container">
                @if (Session::has('errors'))
                    <div class="alert alert-warning" style="width: 80%" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <h5 class="text-center "><span class="glyphicon glyphicon-thumbs-up"></span> {{ $error }}</h5>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">{{$branch->name}}</div>
                            <div class="panel-body">
                                <table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                                    <thead>
                                    <tr>
                                        <th data-field="id" data-sortable="true" >Nro de control</th>
                                        <th data-field="date" data-sortable="true" >Fecha</th>
                                        <th data-field="edit" data-sortable="false">Accion</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($purchases as $sucursal)
                                        <tr>
                                            <td>{{$sucursal->control}}</td>
                                            <td>{{substr($sucursal->created_at,0,10)}}</td>
                                            <td data-featherlight data-featherlight-filter="a" data-featherlight-type="ajax">
                                                <a class="glyphicon glyphicon-eye-open" style="color: orangered" href="{{$url}}/branchoffice/getPurchasesPdf/{{$sucursal->control}}" data-featherlight="iframe" data-featherlight-iframe-allowfullscreen="true" data-featherlight-iframe-width="1000" data-featherlight-iframe-height="600"></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <a href="javascript:history.back()" class="btn btn-info">Atras</a>
                                @if(count($purchases)==0)
                                    {!! Html::link('branchoffice/delete/'.$branch->id, 'Eliminar', array('class' =>'btn btn-danger')) !!}

                                @endif
                            </div>
                        </div>
                    </div>
                </div><!--/.row-->
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('.myElement').featherlight($content, configuration);
    </script>
@stop