@extends('main')
@section('content')

    <style type="text/css">

        header {
            padding: 10px 0;
            margin-bottom: 30px;
        }

        #logo {
            text-align: center;
            margin-bottom: 10px;
        }

        #logo img {
            width: 90px;
        }

        h1 {
            border-top: 1px solid  #5D6975;
            border-bottom: 1px solid  #5D6975;
            color: #5D6975;
            font-size: 2.4em;
            line-height: 1.4em;
            font-weight: normal;
            text-align: center;
            margin: 0 0 20px 0;
            background: url(dimension.png);
        }

        #project {
            float: left;
        }

        #project span {
            color: #5D6975;
            text-align: right;
            width: 52px;
            margin-right: 10px;
            display: inline-block;
            font-size: 0.8em;
        }

        #company {
            float: right;
            text-align: right;
        }

        #project div,
        #company div {
            white-space: nowrap;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            border-spacing: 0;
            margin-bottom: 20px;
        }

        table tr:nth-child(2n-1) td {
            background: #F5F5F5;
        }

        table th,
        table td {
            text-align: center;
        }

        table th {
            padding: 5px 20px;
            color: #5D6975;
            border-bottom: 1px solid #C1CED9;
            white-space: nowrap;
            font-weight: normal;
        }

        table .service,
        table .desc {
            text-align: left;
        }

        table td {
            padding: 20px;
            text-align: right;
        }

        table td.service,
        table td.desc {
            vertical-align: top;
        }

        table td.unit,
        table td.qty,
        table td.total {
            font-size: 1.2em;
        }

        table td.grand {
            border-top: 1px solid #5D6975;;
        }

        #notices .notice {
            color: #5D6975;
            font-size: 1.2em;
        }

        footer {
            color: #5D6975;
            width: 100%;
            height: 30px;
            position: absolute;
            bottom: 0;
            border-top: 1px solid #C1CED9;
            padding: 3px 0 0 0;
            margin-top: 10px;
            text-align: center;
        }
    </style>
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            <div class="container">
                @if (Session::has('errors'))
                    <div class="alert alert-warning" style="width: 80%" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <h5 class="text-center "><span class="glyphicon glyphicon-thumbs-up"></span> {{ $error }}</h5>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Detalle de factura</div>
                            <div class="panel-body">
                                <header class="clearfix">
                                    <div id="logo">
                                        {{ Html::image('image/salud_mental-01.svg','Salud Mental Sucre',array('width' => 200, 'height' => 'auto')) }}
                                    </div>
                                    <h1>Factura nro° {{$branch_id->control}}</h1>
                                    <div id="company" class="clearfix">
                                        <div>Salud Mental Sucre</div>

                                        <div>0293-4317875</div>
                                        <div><a href="mailto:saludmentalsucre@hotmail.com">saludmentalsucre@hotmail.com</a></div>
                                    </div>
                                    <div id="project">
                                        <div><span>Entrega de Medicamentos</span></div>
                                        <div><span>CLIENT</span>{{$branch->name}}</div>

                                        <div><span>EMAIL</span> <a href="mailto:{{$branch->email}}">{{$branch->email}}</a></div>
                                        <div><span>DATE</span> August 17, 2015</div>

                                    </div>
                                </header>
                                <main>
                                    <table>
                                        <thead>
                                        <tr>
                                            <th class="service">Codigo</th>
                                            <th>Nombre</th>
                                            <th>Exp.</th>
                                            <th>Cantidad</th>
                                            <th>Precio U.</th>
                                            <th>Total</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($purchases as $dato)
                                        <tr>
                                            <td class="">{{$dato->codigo}}</td>
                                            <td class="">{{$dato->nombre_c}}</td>
                                            <td class="">{{$dato->expires}}</td>
                                            <td class="">{{$dato->cantidad}}</td>
                                            <td class="">{{$dato->pu}}</td>
                                            <td class="">{{$dato->total}}</td>

                                        </tr>
                                        @endforeach

                                        <tr>
                                            <td colspan="4" class="grand total">TOTAL</td>
                                            <td class="grand total">{{$sum}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div id="notices">
                                        <div data-featherlight data-featherlight-filter="a"
                                             data-featherlight-type="ajax">
                                        <a class="btn btn-large btn-info" href="{{$url}}/branchoffice/getPurchasesPdf/{{$branch_id->control}}" data-featherlight="iframe" data-featherlight-iframe-allowfullscreen="true" data-featherlight-iframe-width="1000" data-featherlight-iframe-height="600">Preparar para Imprimir</a>
                                        </div>
                                        {!! Html::link('/person', 'Terminar', array('class' =>'btn btn-warning')) !!}
                                        <br>
                                        <div>NOTICE:</div>
                                        <div class="notice">Ante cualquier noveda comuniquiese inmediatamente con nosotros</div>
                                    </div>
                                </main>

                                <br>

                            </div>
                        </div>
                    </div>
                </div><!--/.row-->
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('.myElement').featherlight($content, configuration);
    </script>
@stop