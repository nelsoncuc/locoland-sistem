@extends('main')
@section('content')
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            <div class="container">
                @if (Session::has('errors'))
                    <div class="alert alert-warning" style="width: 80%" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <h5 class="text-center "><span class="glyphicon glyphicon-thumbs-up"></span> {{ $error }}</h5>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Sucursales Registradas</div>
                            <div class="panel-body">
                                <table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                                    <thead>
                                    <tr>
                                        <th data-field="control" data-sortable="true" >Nro de control</th>
                                        <th data-field="sucursal" data-sortable="false">Nombre sucursal</th>
                                        <th data-field="edit" data-sortable="false">Accion</th>
                                        <th data-field="in" data-sortable="false">Nuevo envio</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($branch as $sucursal)
                                        <tr>
                                            <td>{{$sucursal->id}}</td>
                                            <td>{{$sucursal->name}}</td>
                                            <td>{!!Html::link('branchoffice/edit/'.$sucursal->id,'', array('class'=>'glyphicon glyphicon-pencil', 'style' => 'margin: 0 10px 0 10px'))!!} | {!!Html::link('branchoffice/listPurchases/'.$sucursal->id,'', array('class'=>'glyphicon glyphicon-eye-open', 'style' => 'margin: 0 10px 0 10px; color:orangered'))!!}</td>
                                            <td>{!!Html::link('branchoffice/register/ini/'.$sucursal->id.'/'.$code,'', array('class'=>'glyphicon glyphicon-shopping-cart', 'style' => 'color: green'))!!}</td>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! Html::link('branchoffice/create', 'Nueva Sucursal', array('class' =>'btn btn-success')) !!}
                            </div>
                        </div>
                    </div>
                </div><!--/.row-->
            </div>
        </div>
    </div>
@stop