<div class="col-lg-12">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">Salida a sucursal: {{$branch->name}}</div>

                <div class="panel-body">
                    <div class="row">
                        <div class="form-group col-lg-4 col-md-4 col-xs-4">
                            <label class="text-primary">N° de Referencia</label>
                            {!!Form::text('codigo', Session::get('codigo'), array('placeholder'=>'Codigo de referencia', 'class' => 'form-control', 'readonly' => 'true' ))!!}<div class="text-danger">{{$errors->first('codigo')}}</div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="form-group col-sm-2">
                            {{ Form::text('id_m', null,array('placeholder'=>'Codigo',  'class' => 'form-control', 'Autofocus')) }}
                            {{ Form::hidden('provider', Session::get('branch'),array( 'class' => 'form-control', 'readonly' => 'true' )) }}
                        </div>
                        <div class="form-group col-sm-1 col-md-1 col-lg-2">

                            {{ Form::text('cantidad', null,array('placeholder'=>'Cantidad',  'class' => 'form-control')) }}
                        </div>
                        <div class="form-group col-sm-1 col-md-1 col-lg-2">

                            {!!Form::text('date_out', '',  ['placeholder'=>'Fecha Venc.','class' => 'form-control datepicker col-md-3 col-xs-12'])!!}
                        </div>
                        <div class="form-group col-sm-1 col-md-1 col-lg-2">
                            {!!Form::submit('Ingresar', array('class' =>'btn btn-info'))!!}
                        </div>
                    </div>



                </div>
            </div>
        </div>
        @if(isset($purchase2))
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Nueva Factura</div>
                            <div class="panel-body">
                                <table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="false" data-show-toggle="false" data-show-columns="false" data-search="false" data-select-item-name="false" data-pagination="false" data-sort-name="false" data-sort-order="asc">
                                    <thead>
                                    <tr>
                                        <th data-field="medicamento" data-sortable="true" >Medicamnto</th>
                                        <th data-field="cantidad" data-sortable="true" >Cantidad</th>
                                        <th data-field="Vencimiento" data-sortable="false">Vencimiento</th>
                                        <th data-field="Precio" data-sortable="false">Precio U.</th>
                                        <th data-field="Total" data-sortable="false">Total</th>
                                        <th data-field="Action" data-sortable="false">Accion</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($purchase2 as $facturas)
                                        <tr>
                                            <td>{{$facturas->nombre}}</td>
                                            <td>{{$facturas->cantidad}}</td>
                                            <td>{{$facturas->expires}}</td>
                                            <td>{{$facturas->pu}}</td>
                                            <td>{{$facturas->total}}</td>
                                            <td>{!!Html::link('branchoffice/register/delete/'.$facturas->idp,'', array('class'=>'glyphicon glyphicon-remove', 'style'=>'color: red'))!!}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div style="float: right; padding-top: 10px">{!! Html::link('branchoffice/register/add/'.Session::get('codigo'), 'Finalizar', array('class' =>'btn btn-info')) !!}</div>
                            </div>

                        </div>
                    </div>
                </div><!--/.row-->
            </div>
        @endif
    </div><!--/.row-->
</div>

