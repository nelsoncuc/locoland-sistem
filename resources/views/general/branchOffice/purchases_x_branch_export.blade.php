<header class="clearfix">
    <div id="logo">

    </div>
    <header class="clearfix">
        <div id="logo">

        </div>
        <h1>Factura nro° {{$branch_id->control}}</h1>
        <div id="company" class="clearfix">
            <div>Salud Mental Sucre</div>

            <div>0293-4317875</div>
            <div><a href="mailto:saludmentalsucre@hotmail.com">saludmentalsucre@hotmail.com</a></div>
        </div>
        <div id="project">
            <div><span>Entrega de Medicamentos</span></div>
            <div><span>CLIENT</span>{{$branch->name}}</div>
            @if(count($branch->email)!=0)
                <div><span>EMAIL</span> <a href="mailto:{{$branch->email}}">{{$branch->email}}</a></div>
            @endif
            <div><span>DATE</span> August 17, 2015</div>

        </div>
    </header>
    <main>
        <table>
            <thead>
            <tr>
                <th class="service">Codigo</th>
                <th>Nombre</th>
                <th>Exp.</th>
                <th>Cantidad</th>
                <th>Precio U.</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach($purchases as $dato)
                <tr>
                    <td class="">{{$dato->codigo}}</td>
                    <td class="">{{$dato->nombre_c}}</td>
                    <td class="">{{$dato->expires}}</td>
                    <td class="">{{$dato->cantidad}}</td>
                    <td class="">{{$dato->pu}}</td>
                    <td class="">{{$dato->total}}</td>

                </tr>
            @endforeach

            <tr>
                <td colspan="4" class="grand total">TOTAL</td>
                <td class="grand total">{{$sum}}</td>
            </tr>
            </tbody>
        </table>
        <div id="notices">
            <input type="button" value="Imprime esta pagina" onclick="window.print()">
            <div>NOTICE:</div>
            <div class="notice">Ante cualquier noveda comuniquiese inmediatamente con nosotros</div>
        </div>
    </main>
</header>
