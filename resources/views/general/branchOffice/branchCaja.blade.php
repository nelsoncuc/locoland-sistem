@extends('main')
@section('content')

    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
           @if (Session::has('errors'))
                <div class="alert alert-warning" style="width: 100%" role="alert">
                    <ul>
                        @foreach ($errors->all() as $mesage)
                            <h5 class=""><span class="glyphicon glyphicon-thumbs-up"></span> {{ $mesage }}</h5>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="col-lg-12">

                <div  class="" style="margin-left: 10px">
                    <div class="row">

                    </div>
                    <div class="row co-lg-12 col-md-12 col-sm-12">
                        <br>
                        {!! Form::open(['route'=>'branchoffice/register/register_med', 'method' => 'post', 'class' => 'form-inline']) !!}
                        {{csrf_field()}}
                        @include('general.branchOffice.ingre_caja')
                        {!! Form::close() !!}
                        <br>
                        {{-- @include('general.branchOffice.ingre_date_caja')
                     --}}</div>
                </div>
            </div>
        </div>
    </div>

@endsection

