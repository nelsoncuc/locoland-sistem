@extends('main')
@section('content')
    <div class="row">
        <div class="col-sm-12 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Glosario de Medicamentos</div>
                            <div class="panel-body">
                                <table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                                    <thead>
                                    <tr>
                                        <th data-field="nombre_c" data-sortable="true" >Nombre Comercial</th>
                                        <th data-field="principio_a" data-sortable="true">Principio Activo</th>
                                        <th data-field="presentacion"  data-sortable="true">Presentacion</th>
                                        <th data-field="cantidad" data-sortable="false">Cantidad</th>
                                        <th data-field="Farmaceutica" data-sortable="false">Farmaceutica</th>
                                        <th data-field="Indicaciones" data-sortable="false">indicaciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($medicine as $m)
                                        <tr>
                                            <td>{{$m->nombre_c}} {{$m->composicion}}</td>
                                            <td>{{$m->principio_a}}</td>
                                            <td>{{$m->presentacion}}</td>
                                            <td>{{$m->cantidad}}</td>
                                            <td>{{$m->farmaceutica}}</td>
                                            <td><a type="button" class="btn btn-default btn-sm" data-container="body"
                                                   data-html="true"
                                                   data-toggle="popover" data-placement="left"  data-content=" <spam style='width: 500px'>{{$m->info}}</spam> ">
                                                    Ver Info
                                                </a>
                                                </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!--/.row-->
            </div>
        </div>
    </div>
    <script>
        $(function () {
            $('[data-toggle="popover"]').popover()
        })
    </script>
@stop