@extends('main')
@section('content')
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            <div class="col-lg-12">
                <div>
                    <h4 class="text-light-blue" style="padding-left: 16px">Nueva Compra</h4>
                    <div class="divider">
                    </div>
                </div>
                <div  class="panel panel-default" style="margin-left: 10px">
                    {!! Form::open(['route' => 'person.store', 'method' => 'POST']) !!}
                    <div class="row" style="padding: 10px">
                        @include('general.person.partials.shop')
                        <div class="col-md-12">{!!Form::submit('Registrar', ['class' => "btn btn-primary"])!!}</div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop