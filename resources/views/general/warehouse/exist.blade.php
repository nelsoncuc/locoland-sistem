@extends('main')
@section('content')
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            <div class="container">
                @if (Session::has('errors'))
                    <div class="alert alert-warning" style="width: 80%" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <h5 class="text-center "><span class="glyphicon glyphicon-thumbs-up"></span> {{ $error }}</h5>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Existencia en Almacen</div>
                            <div class="panel-body">
                                <table data-toggle="table" data-url="tables/data1.json"  data-show-refresh="true" data-show-toggle="true" data-show-columns="true" data-search="true" data-select-item-name="toolbar1" data-pagination="true" data-sort-name="name" data-sort-order="desc">
                                    <thead>
                                    <tr>
                                        <th data-field="nombre" data-sortable="true" >Medicamento</th>
                                        <th data-field="fecha ultima" data-sortable="false">Fecha Ult. Movimiento</th>
                                        <th data-field="cod. ultima"  data-sortable="false">Cod. Ult. Movimiento</th>
                                        <th data-field="existencia" data-sortable="false">Existencia</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                      @foreach($exist as $existencia)
                                        <tr>
                                            <td>{{$existencia->nombre_c}} {{$existencia->composicion}}</td>
                                            <td>{{$existencia->created_at}}</td>
                                            <td>{{$existencia->entrega_id}}</td>
                                            <td>{{$existencia->total}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!--/.row-->
            </div>
        </div>
    </div>
@stop