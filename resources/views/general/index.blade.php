@extends('main')
@section('content')

    <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 10px 0 10px">
        @include('pages.message')
        <div class="row" style="padding-left: 16px">
                @include('general.widgets.totalpatient')
                @include('general.widgets.total_todayw')
            </div>
            <div class="row">
                @include('general.widgets.person_today')

        </div><!-- /.row -->

    </div><!--/.main-->
    <script>
        $(function () {
            $('#hover, #striped, #condensed').click(function () {
                var classes = 'table';

                if ($('#hover').prop('checked')) {
                    classes += ' table-hover';
                }
                if ($('#condensed').prop('checked')) {
                    classes += ' table-condensed';
                }
                $('#table-style').bootstrapTable('destroy')
                        .bootstrapTable({
                            classes: classes,
                            striped: $('#striped').prop('checked')
                        });
            });
        });

        function rowStyle(row, index) {
            var classes = ['active', 'success', 'info', 'warning', 'danger'];

            if (index % 2 === 0 && index / 2 < classes.length) {
                return {
                    classes: classes[index / 2]
                };
            }
            return {};
        }
    </script>
@endsection
