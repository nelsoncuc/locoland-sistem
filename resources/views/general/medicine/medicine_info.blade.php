@extends('main')
@section('content')
    <div class="row">
        <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
            <div class="col-lg-12">
                <div>
                    <h4 class="text-light-blue" style="padding-left: 16px">Detalle de Medicamento</h4>
                    <div class="divider">
                    </div>
                </div>
                <div  class="panel panel-default" style="margin-left: 10px">
                    <div class="col-md-5">
                        <div class="list-group">
                            <li class="list-group-item list-group-item-info"><h4 class="text-capitalize">{{$datos->nombre_c}} {{$datos->composicion}}</h4></li>
                            <li class="list-group-item">Codigo:  <strong>{{$datos->codigo}}<br></strong></li>
                            <li class="list-group-item">Principio Activo: {{$datos->principio_a}}  <br>
                            <li class="list-group-item text-capitalize">Presentacion: {{$datos->presentacion}}  </li>
                            <li class="list-group-item text-capitalize">Farmaceutica: {{$datos->farmaceutica}}</li>
                            <li class="list-group-item text-capitalize">Cantidad: {{$datos->cantidad}}</li>
                            <li class="list-group-item text-capitalize">Informacion:{{$datos->info}} </li>

                        </div>
                        <div class="row" style="padding-left: 16px">
                            {!! Html::link('/paciente', 'regresar', array('class' =>'btn btn-primary')) !!}
                            <a href="{{route('paciente.edit', $datos->id)}}" class="btn btn-primary">Editar</a>

                            {!! Html::link('person/caja/'.$datos->id, 'Nuevo', array('class' =>'btn btn-success')) !!}

                        </div>

                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop



