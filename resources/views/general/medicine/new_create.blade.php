@extends('main')
@section('content')
<div class="row">
                <div class="col-sm-9 col-sm-offset-3 col-lg-9 col-lg-offset-2 main" style="margin: 10px 0 0 10px;">
                    <div class="col-lg-12">
                        <div class="col-md-8">
                            <div class="panel panel-default">
                                <div class="panel-heading">Registrar Medicamento</div>
                                <div class="panel-body">
                                    {!! Form::open(['route' => '/warehouse/medicine/add/store', 'method' => 'POST']) !!}
                                    <div class="row" style="padding: 10px">
                                        @include('general.person.partials.Medicinefields')
                                        <div class="col-md-12">{!!Form::submit('Registrar', ['class' => "btn btn-primary"])!!}</div>
                                        {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
@stop
