{!! Form::open(['route' => ['caja.destroy', $retir->id], 'method' => 'DELETE']) !!}
<button type="submit" onclick="return confirm('Seguro desea eliminar este elemento?')" class="btn btn-danger glyphicon glyphicon-remove"></button>
{!! Form::close() !!}