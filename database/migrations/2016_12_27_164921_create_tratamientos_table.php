<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTratamientosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tratamientos', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->integer('id_h')->unsigned();//
            $table->integer('medicamento_id')->unsigned();//
            $table->integer('catidad');//

            $table->timestamps();
        });

        Schema::table('tratamientos', function(Blueprint $table)
        {

            $table->foreign('medicamento_id')->references('id')
                ->on('medicamentos')
                ->onDelete('No Action')
                ->onUpdate('No Action');
        });

        Schema::table('tratamientos', function(Blueprint $table)
        {

            $table->foreign('id_h')->references('id')
                ->on('historias')
                ->onDelete('No Action')
                ->onUpdate('No Action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tratamientos');
    }
}
