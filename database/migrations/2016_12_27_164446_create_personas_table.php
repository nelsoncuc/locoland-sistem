<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->string('nombre',45);// nombre
            $table->string('documento',20); // documento de identificacion
            $table->date('fecha_nac')->nullable();// fecha de nacimiento
            $table->string('sexo', 10); // sexo
            $table->string('direccion',1500)->nullable(); //telefono casa
            $table->string('telefono',20)->nullable();// telefono movil
            $table->string('email',45)->nullable();// email

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('personas');
    }
}
