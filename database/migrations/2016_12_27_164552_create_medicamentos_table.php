<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicamentos', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->string('codigo',45);
            $table->string('nombre_c',45);
            $table->string('principio_a',45)->nullable();
            $table->string('presentacion',45);
            $table->string('farmaceutica',45);
            $table->integer('cantidad');
            $table->string('composicion',45);
            $table->string('info',100)->nullable();//
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medicamentos');
    }
}
