<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandofficeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_office', function(Blueprint $table)
        {
            $table->increments('id')->unsigned(); // id de la persona
            $table->string('name',100);// nombre
            $table->string('rif',50); //
            $table->string('phone', 50); //
            $table->string('email',45);// email

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('branch_office');
    }
}
