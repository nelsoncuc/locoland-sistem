<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntregasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entregas', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->integer('persona_id')->unsigned();//
            $table->integer('cantidad');//
            $table->date('fecha');//
            $table->integer('medicamento_id')->unsigned();//
            $table->date('prox_fecha'); //
            $table->timestamps();
        });

        Schema::table('entregas', function(Blueprint $table)
        {
    
            $table->foreign('persona_id')->references('id')
                ->on('personas')
                ->onDelete('No Action')
                ->onUpdate('No Action');


        });
        Schema::table('entregas', function(Blueprint $table)
        {
           
            $table->foreign('medicamento_id')->references('id')
                ->on('medicamentos')
                ->onDelete('No Action')
                ->onUpdate('No Action');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('entregas');
    }
}
