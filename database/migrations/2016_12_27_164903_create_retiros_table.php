<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetirosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('retiros', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->integer('catidad');//
            $table->integer('medicamento_id')->unsigned();//
            $table->integer('persona_id')->unsigned();//

            $table->timestamps();
        });

        Schema::table('retiros', function(Blueprint $table)
        {

            $table->foreign('persona_id')->references('id')
                ->on('personas')
                ->onDelete('No Action')
                ->onUpdate('No Action');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('retiros');
    }
}
