<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicos', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->string('nombrem',45);//
            $table->integer('documento');//
            $table->string('especialidad',45);//
            $table->string('msds',45);//
            $table->string('sexo',45);//
            $table->string('telefono', 11); //
            $table->string('email',45);//
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medicos');
    }
}
