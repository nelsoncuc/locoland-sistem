<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumMunicioPerson extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('personas', function(Blueprint $table)
        {
            $table->integer('municipio')->unsigned();

        });
        Schema::table('personas', function(Blueprint $table)
        {

            $table->foreign('municipio')->references('id')
                ->on('municipios')
                ->onDelete('No Action')
                ->onUpdate('No Action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropColumn('municipio');

    }
}
