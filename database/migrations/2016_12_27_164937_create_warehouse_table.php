<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWarehouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouse', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->integer('id_m')->unsigned();//
            $table->string('entrega_id', 20)->unsigned();//
            $table->date('date');//
            $table->integer('in_w');//
            $table->integer('out_w');//
            $table->string('total',11);//

            $table->timestamps();
        });

        Schema::table('warehouse', function(Blueprint $table)
        {

            $table->foreign('id_m')->references('id')
                ->on('medicamentos')
                ->onDelete('No Action')
                ->onUpdate('No Action');
        });

        Schema::table('warehouse', function(Blueprint $table)
        {

            $table->foreign('entrega_id')->references('id')
                ->on('entregas')
                ->onDelete('No Action')
                ->onUpdate('No Action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('warehouse');
    }
}
