<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMunicipiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('municipios', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->string('nombre',45);//
            $table->integer('id_estado')->unsigned();//

            $table->timestamps();
        });

        Schema::table('municipios', function(Blueprint $table)
        {

            $table->foreign('id_estado')->references('id')
                ->on('estados')
                ->onDelete('No Action')
                ->onUpdate('No Action');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('municipios');
    }
}
