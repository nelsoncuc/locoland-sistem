<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchases', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->integer('control');
            $table->integer('medicamento_id')->unsigned();//
            $table->integer('cantidad');//
            $table->date('expires');//
            $table->integer('provider_id')->unsigned();//
            $table->integer('pu');//
            $table->string('total',12);//

            $table->timestamps();
        });

        Schema::table('purchases', function(Blueprint $table)
        {

            $table->foreign('medicamento_id')->references('id')
                ->on('medicamentos')
                ->onDelete('No Action')
                ->onUpdate('No Action');
        });

        Schema::table('purchases', function(Blueprint $table)
        {

            $table->foreign('provider_id')->references('id')
                ->on('provider')
                ->onDelete('No Action')
                ->onUpdate('No Action');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchases');
    }
}
