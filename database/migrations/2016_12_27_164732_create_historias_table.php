<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historias', function(Blueprint $table)
        {
            $table->increments('id')->unsigned();
            $table->integer('persona_id')->unsigned();//
            $table->string('diagnostico',500)->nullable();//
            $table->integer('medico_id')->unsigned();//
            $table->timestamps();
        });

        Schema::table('historias', function(Blueprint $table)
        {

            $table->foreign('persona_id')->references('id')
                ->on('personas')
                ->onDelete('No Action')
                ->onUpdate('No Action');


        });
        Schema::table('historias', function(Blueprint $table)
        {
            //
            $table->foreign('medico_id')->references('id')
                ->on('medicos')
                ->onDelete('No Action')
                ->onUpdate('No Action');
        });


}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('historias');
    }
}
