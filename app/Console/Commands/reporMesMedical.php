<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\URL;

class reporMesMedical extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:medicine';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'reporte mensual de medicamentos entregados';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date_in = Carbon::now()->subDays(31)->format('Y-m-d');
        $today = Carbon::now();
        $next_in  = $today->format('Y-m-d');

        $url =getenv('URL');

        $email   = 'saludmentalsucre2010@hotmail.com';
        $message = $url.'/export/getReport30/'.$date_in.'/2/'.$next_in;

        $data['email']      = $email;
        $data['message']    = $message;
        $data['url']        = $url;


        Mail::send('emails.send_medi30', [ 'data' => $data ], function ($mail) use ($data) {

            $mail->subject('Salud Mental Sucre (reporte medicamentos)');
            $mail->from('informacion@saludmentalsucre.com.ve', 'Salud Mental Sucre');
            $mail->priority('urgent');
            $mail->to($data['email']);

        });

    }
}
