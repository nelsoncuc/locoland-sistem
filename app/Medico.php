<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Medico extends Model {

    protected $table = 'medicos';
	//
    public function medicoH()
    {
        return $this->hasMany('App/Historia', 'id');
    }
    public function medico(){

        return $this->belongsTo('app\Medico', 'medico_id', 'id');

    }
}
