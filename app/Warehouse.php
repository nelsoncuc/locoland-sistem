<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'warehouse';
    protected $fillable = ['id_m', 'total', 'date'];

    public function medicamento() {
        return $this->belongsTo('App\Medicament','id_m','id' );
    }
}
