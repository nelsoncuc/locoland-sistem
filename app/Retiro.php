<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Retiro extends Model {

    protected $table = 'retiros';
    protected $fillable = ['cantidad', 'medicamento_id', 'person_id'];

    public function medicamento() {
        return $this->belongsTo('App\Medicament','medicamento_id','id' );
    }

}
