<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branchoffice extends Model
{
    protected $table = 'branch_office';
    protected $fillable = ['name', 'rif', 'phone','email'];
}
