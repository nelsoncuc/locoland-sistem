<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Entrega extends Model {

    protected $table = 'entregas';

    public function userEntrega()
    {
        return $this->belongsTo('app\Persona','persona_id','id');
    }

    public function entreHistoria()
    {
        return $this->belongsTo('app\Persona','persona_id','id');
    }

    public function medicamento()
    {
        return $this->belongsTo('App\Medicament','medicamento_id','id' );
    }
}
