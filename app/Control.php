<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Control extends Model {

    protected $table = 'control';
    protected $fillable = ['persona_id', 'fecha', 'nro_control'];

}
