<?php

namespace App\Http\Controllers;

use App\Branchoffice;
use App\Http\Requests\branchOfficeRequest;
use App\Http\Requests\registerBranchRequest;
use App\Medicament;
use App\Purchases;
use App\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request as Recu;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use Illuminate\Support\Facades\URL;

class branchOfficeController extends Controller
{
    public function index(){

        $branch = branchoffice::all();
        $code = date('dmyhms');
        return view('general/branchOffice/index', compact("branch", "code"));
    }

    //**************creando, editando, eliminando sucrsales*******************//
    public function create()
    {
        return view('general/branchOffice/create');
    }

    public function store(branchOfficeRequest $request){

        $branch = new Branchoffice();
        $branch->name = $request->name;
        $branch->rif = $request->rif;
        $branch->phone = $request->phone;
        $branch->email = $request->email;
        $branch->save();

        $branch = branchoffice::all();
        $code = date('dmyhms');
        return view('general/branchOffice/index', compact("branch", "code"));

    }

    public function edit($id){
        $dato= Branchoffice::findOrFail($id);
        return view('general/branchOffice/edit', compact("dato"));
    }


    public function update($id){

        $branch = Branchoffice::findOrFail($id);
        $branch->fill(Recu::all());
        $branch->save();
        $branch = branchoffice::all();
        $code = date('dmyhms');
        return view('general/branchOffice/index', compact("branch", "code"));
    }

    //***************************fin sucrsales CRUD*********************//

    //************************retiro medicamentos alamcen a sucrsales***********//
    public function register_ini($id, $code){
        $branch = Branchoffice::where('id', $id)->first();
        Session::put('codigo', $code);
        Session::put('branch', $id);
        $purchase2 = \DB::table('purchases')
            ->join('medicamentos', 'medicamentos.id','=', 'id_m')
            ->select('medicamentos.nombre_c as nombre', 'purchases.id as idp','purchases.control', 'purchases.cantidad', 'purchases.expires', 'purchases.pu',
                'purchases.total', 'purchases.branch_id')
            ->where('purchases.branch_id',Session::get('branch', $id))
            ->where('purchases.control', Session::get('codigo', $code))
            ->where('purchases.status', '=','branch_pendig')
            ->get();

        return view('general/branchOffice/branchCaja', compact("branch", "purchase2"));
    }

    public function register_med(registerBranchRequest $request){

        $codigo=Input::get('id_m');
        $branch=Input::get('provider');
        $reference=Session::get('codigo');

        $medical = Medicament::where('codigo', $codigo)->first();
        $warehouse = Warehouse::where('id_m', $medical->id)
            ->orderBy('id','desc')
            ->first();

        if ($request->cantidad > $warehouse->total){
            return redirect()->back()->withErrors(['No hay existencia la cantidad de producto que solicita' ]);
        }elseif ($request->cantidad == 0){
            return redirect()->back()->withErrors(['debe introducir una cantidad superior a cero "0"' ]);
        }

        $purch = \DB::table('purchases')
            ->join('medicamentos', 'medicamentos.id','=', 'id_m')
            ->select('medicamentos.codigo', 'medicamentos.id','purchases.id_m', 'purchases.pu', 'purchases.cantidad')
            ->where('medicamentos.codigo', '=',$codigo)
            ->orderBy('id','desc')
            ->first();

            $campos = Recu::all();
            $purchase = new Purchases();
            $purchase->control = $reference;
            $purchase->id_m = $purch->id_m;
            $purchase->cantidad = $campos["cantidad"];
            $purchase->expires = $campos["date_out"];
            $purchase->provider_id = '0';
            $purchase->pu = $purch->pu;
            $purchase->total = $campos["cantidad"]*$purch->pu;
            $purchase->status = 'branch_pendig';
            $purchase->branch_id = $branch;
            $purchase->save();

            return redirect()->back();

    }

    public function delete($id){

        \DB::table('purchases')->where('id', '=', $id)->delete();
        return redirect()->back();
    }

    public function getFinal(){
        $control =  Session::get('codigo');
        $date=date('Y-m-d');
        $purchases = Purchases::where('control', $control)
            ->where('status', 'branch_pendig')
            ->get();
        if (count($purchases)==0){
            return redirect()->back()->withErrors(['Debe ingresar algún producto' ]);
        }
        foreach ($purchases  as $test){
            $almacen = Warehouse::where('id_m',$test->id_m)->orderBy('id', 'DESC')->first();


                $total = $almacen->total - $test->cantidad;
                $warehouse = new Warehouse();
                $warehouse->id_m = $test->id_m;
                $warehouse->entrega_id = $test->control;
                $warehouse->in_w ='0';
                $warehouse->out_w =$test->cantidad;
                $warehouse->total = $total;
                $warehouse->date = $date;
                $warehouse->save();

        }
        foreach ($purchases  as $test){
            \DB::table('purchases')->where('control', '=', $test->control)->update([
                'status' => 'branch_ready',
            ]);
        }
        $purchases = \DB::table('purchases')
            ->join('medicamentos', 'medicamentos.id','=', 'id_m')
            ->select('medicamentos.codigo', 'medicamentos.id', 'medicamentos.nombre_c','purchases.id_m', 'purchases.pu', 'purchases.total','purchases.cantidad','purchases.expires')
            ->where('purchases.control', '=',$control)
            ->where('status','=','branch_ready')
            ->get();
        $branch_id= Purchases::where('control', $control)
            ->first();
        $branch = Branchoffice::where('id', $branch_id->branch_id)->first();
        $collection = collect($purchases);
        $sum = $collection->sum('total');
        $url =URL::to('/');

        $email   = $branch->email;
        $message = $url.'/export/getpuexport/'.$control ;

        $data['email']      = $email;
        $data['message']    = $message;
        $data['url']        = $url;


        Mail::send('emails.send_message', [ 'data' => $data ], function ($mail) use ($data) {

            $mail->subject('Salud Mental Sucre');
            $mail->from('informacion@saludmentalsucre.com.ve', 'Salud Mental Sucre');
            $mail->priority('urgent');
            $mail->to($data['email']);

        });
        return view('general/branchOffice/purchases',compact("purchases", "branch", "branch_id", "sum", "url"));
    }

    public function getPurchasesPdf($id){

        $purchases = \DB::table('purchases')
            ->join('medicamentos', 'medicamentos.id','=', 'id_m')
            ->select('medicamentos.codigo', 'medicamentos.id', 'medicamentos.nombre_c','purchases.id_m', 'purchases.pu', 'purchases.total','purchases.cantidad','purchases.expires')
            ->where('purchases.control', '=',$id)
            ->where('status','=','branch_ready')
            ->get();
        $branch_id= Purchases::where('control', $id)
            ->first();
        $branch = Branchoffice::where('id', $branch_id->branch_id)->first();
        $collection = collect($purchases);
        $sum = $collection->sum('total');
        return view('general/report/views/pdfpurchases',compact("purchases", "branch", "branch_id", "sum"));
    }

    public function getListPurchases($id){
        $purchases = Purchases::where('branch_id', $id)
            ->where('status', 'branch_ready')
            ->groupBy('control')
            ->get();
        $branch = Branchoffice::where('id', $id)->first();
        $url =URL::to('/');
        return view('general/branchOffice/purchases_x_branch',compact("purchases", "url", "branch"));
    }

    public function deleteBranch($id){
        \DB::table('branch_office')->where('id', '=', $id)->delete();
        $branch = branchoffice::all();
        $code = date('dmyhms');
        return view('general/branchOffice/index', compact("branch", "code"));
    }
}
