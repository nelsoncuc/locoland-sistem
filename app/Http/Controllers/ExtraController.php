<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterMedicineRequest;
use App\Medicament;
use App\Warehouse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;

use App\Http\Requests;
use Illuminate\Support\Facades\Request as Recu;
class ExtraController extends Controller
{
    public function create(){
        return view('general/medicine/new_create');
    }

    public function store(RegisterMedicineRequest $request){
        $validator = Validator::make($request->all(), $request->rules(), $request->messages());
        If ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            $campos = Recu::all();

            $medicine= new Medicament();
            $medicine->codigo=$campos["codigo"];
            $medicine->nombre_c=$campos["nombre_c"];
            $medicine->principio_a=$campos["principio_a"];
            $medicine->presentacion=$campos["presentacion"];
            $medicine->farmaceutica=$campos["farmaceutica"];
            $medicine->cantidad=$campos["cantidad"];
            $medicine->composicion=$campos["composicion"];
            $medicine->info=$campos["info"];
            $medicine->save();

            $datos = Medicament::where('codigo', $campos["codigo"])->first();
            /*----------se crea el nuevo medicamento con valor 0 en almacen**********/
            $date=date('Y-m-d');

            $warehouse = new Warehouse();
            $warehouse->id_m = $datos->id;
            $warehouse->entrega_id = 'New';
            $warehouse->in_w ='0';
            $warehouse->out_w ='0';
            $warehouse->total = '0';
            $warehouse->date = $date;
            $warehouse->save();
            
            return view("general/medicine/final_create")->withErrors(['Registro ingresado con exito.']);


        }

    }
}
