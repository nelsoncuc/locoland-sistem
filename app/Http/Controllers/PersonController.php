<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class PersonController extends Controller
{

    public function create()
    {
        $medic_option = \DB::table('medicos')->orderBy('nombrem', 'desc')->lists('nombrem','id');
        $state_option = \DB::table('estados')->orderBy('id', 'asc')->lists('nombre','id');
        $muni_option = \DB::table('municipios')->orderBy('id', 'asc')->lists('nombre','id','id_estado');
        return view('person.create', array ('medic_option' => $medic_option,'state_option' => $state_option, 'muni_option' => $muni_option));
    }

    /*+++++++++++++++++++++++++++++INSERTAR NUEVA PERSON++++++++++++++++++++++++*/
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
/*    public function store()
    {
        $campos= Request::all();

        $paciente=new Persona;
        $paciente->nombre=$campos["nombre"];
        $paciente->documento=$campos["documento"];
        $paciente->fecha_nac=$campos["fecha_nac"];
        $paciente->sexo=$campos["sexo"];
        $paciente->municipio=$campos["municipios"];
        $paciente->direccion=$campos["direccion"];
        $paciente->telefono=$campos["telefono"];
        $paciente->save();

        $idp=$paciente->id;

        $historia=new Historia();
        $historia->persona_id=$idp;
        $historia->diagnostico=$campos["diagnostico"];
        $historia->medico_id=$campos["medicos"];
        $historia->save();
        $datos=Persona::find($idp);
        $pip = \DB::table('entregas')
            ->Join('control', 'control.persona_id', '=', 'entregas.persona_id')
            ->where('control.persona_id', '=',$datos->id)
            ->groupBy('control.fecha')
            ->get();
        $history =  Entrega::where('persona_id', '=', $datos->id)->paginate(6);
        return view('person.show', compact("datos", "pip"));
    }*/

}
