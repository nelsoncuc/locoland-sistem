<?php

namespace App\Http\Controllers;


use App\Entrega;

use App\Http\Requests\RegisterCajaRequest;
use App\Warehouse;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Persona;
use Illuminate\Support\Facades\Request;
use App\Http\Requests;

use App\Medicament;
use App\Retiro;

use Validator;
class CajaController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		if(Request::has('date_in')){
			$campos= Request::all();
		} else {
			return redirect()->back();
		}

		/*$d_in =substr(Input::get('date_in'), 0, 10);
		$date_in =new Carbon($d_in);
		$n_in =substr(Input::get('date_in'), 13, 23);
		$next_in = new Carbon($n_in);
*/
		$date=date('Y-m-d');
		$d_in = Input::get('date_in');
		$date_in =new Carbon($d_in);
		$n_in = Input::get('date_out');
		$next_in = new Carbon($n_in);

		$person_id =  $campos['id'];
		$medi = Retiro::where('person_id', $person_id)->get();


		foreach ($medi as $in){
			$entrega=new Entrega();
			$entrega->persona_id=$person_id;
			$entrega->cantidad=$in->cantidad;
			$entrega->fecha=$date_in->format('Y-m-d');
			$entrega->medicamento_id=$in->medicamento_id;
			$entrega->prox_fecha=$next_in->format('Y-m-d');
			$entrega->save();

		}
		$nentregas = \DB::table('entregas')->select('id')->orderby('id','DESC')->take(1)->get();
		$a =$nentregas[0];
		foreach ($medi as $test){
			$almacen = Warehouse::where('id_m',$test->medicamento_id)->orderBy('id', 'DESC')->first();
			$total = $almacen->total - $test->cantidad;

			$warehouse = new Warehouse();
			$warehouse->id_m = $test->medicamento_id;
			$warehouse->entrega_id = $a->id;
			$warehouse->in_w ='0';
			$warehouse->out_w =$test->cantidad;
			$warehouse->total = $total;
			$warehouse->date = $date;
			$warehouse->save();

		}

		Retiro::where('person_id', $person_id)->delete();
		/*retorno al index*/
		$result = Persona::all()->count();


		$date=date('Y-m-d');
		$person = Persona::all()->count();

		$out = Entrega::distinct()->select('persona_id')
			->where('prox_fecha', '=', $date)->get()
			->count();

		$entregas = Entrega::where('prox_fecha', '=', $date)
			->groupBy('persona_id')
			->get();
		return \View::make("general/index", compact("person", "entregas", "out"));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RegisterCajaRequest $request)
	{
		$validator = Validator::make($request->all(), $request->rules(), $request->messages());
		If ($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput();
		} else {

			$campos = Request::all();
			$medicine = Medicament::where('codigo', '=', $campos['codigo'])->first();

			$w = Warehouse::where('id_m',$medicine->id)->orderBy('id', 'DESC')->first();

			if ($w == null){
				return redirect()->back()->withErrors(['No hay existencia de este medicamento en almacen' ]);
			}elseif ($w->total <= 0){
				return redirect()->back()->withErrors(['No hay existencia de este medicamento en almacen' ]);
			}

			$medi = $medicine->id;
			$entrega = new Retiro;
			$entrega->person_id = $campos['person'];
			$entrega->cantidad = $campos["cantidad"];
			$entrega->medicamento_id = $medi;
			$entrega->save();
			//$datos=Session::get('id');
			return redirect()->back();
		}
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

		Retiro::destroy($id);

		Session::flash('message', 'El evento fue eliminado');

		return redirect()->back();
	}
	public function regis($id)
	{
		//
	}
}
