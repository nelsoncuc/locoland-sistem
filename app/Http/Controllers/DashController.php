<?php

namespace App\Http\Controllers;

use App\Entrega;

use App\Historia;
use App\Http\Requests\RegisterUserRequest;
use App\Medico;
use App\Persona;
use App\Warehouse;
use Carbon\Carbon;
use Composer\Autoload;
use App\Retiro;
use Mailgun\Mailgun;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

use Validator;

class DashController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mgClient = new Mailgun('key-ae035b9e58dbc039fdc47fe792c73d3d');
        $domain = "saludmentalsucre.com.ve";

# Make the call to the client.
       /* $result = $mgClient->sendMessage($domain, array(
            'from'    => 'test@saludmenatlsucre.com.ve',
            'to'      => 'Baz nelsoncuc@hotmail.com',
            'subject' => 'Hello_test mode',
            'text'    => 'Testing some Mailgun awesomness!',
            'o:testmode' => true
        ));*/
        # Issue the call to the client.
        $result = $mgClient->get("lists/pages", array(
            'limit'      =>  5
        ));
        //dd($result->http_response_body->items[0]);
     

     
        $person = Persona::all()->count();
        $date=date('Y-m-d');
        $entregas = Entrega::where('prox_fecha', '=', $date)
            ->groupBy('persona_id')

            ->get();
        $out = Entrega::distinct()->select('persona_id')
            ->where('prox_fecha', '=', $date)->get()
            ->count();
        return \View::make("general/index", compact("person", "entregas", "out"));
    }

    /*+++++++++++++++++++++++++++++DETALLE DE PERSONAS++++++++++++++++++++++++*/
    public function get_detail($id=null)
    {
        $datos=Persona::find($id);
        $history =  Entrega::where('persona_id', '=', $datos->id)->paginate(8);
        $medico_id = Historia::where('persona_id', '=', $datos->id)->first();
        $medico = Medico::where('id', '=', $medico_id->id)->first();

        $pip = \DB::table('entregas')
            ->join('personas', 'personas.id','=', 'entregas.persona_id')
            ->join('medicamentos', 'medicamentos.id', '=', 'entregas.medicamento_id')
            ->join('historias', 'historias.persona_id', '=', 'entregas.persona_id')
            ->join('medicos', 'medicos.id', '=', 'historias.medico_id')
            ->select('entregas.cantidad', 'entregas.persona_id', 'entregas.medicamento_id', 'entregas.fecha', 'entregas.prox_fecha',
                'medicamentos.nombre_c','medicamentos.principio_a', 'medicamentos.composicion', 'personas.*', 'historias.medico_id',
                'medicos.nombrem')
            ->where('entregas.persona_id', '=',$id)
            ->orderBy('entregas.fecha', 'DESC')
            ->paginate(10);
        

        return \View::make('general/person/detail', compact("datos", "history", "medico", "pip"));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        
        $medic_option = \DB::table('medicos')->orderBy('nombrem', 'desc')->lists('nombrem','id');
        $state_option = \DB::table('estados')->orderBy('id', 'asc')->lists('nombre','id');
        $muni_option = \DB::table('municipios')->orderBy('id', 'asc')->lists('nombre','id','id_estado');
        $datos= Persona::findOrFail($id);

        $datos_H = Historia::where('persona_id', $datos->id)->first();


        return \View::make("general/person/edit", array ('medic_option' => $medic_option,'state_option' => $state_option, 'muni_option' => $muni_option), compact("datos", "datos_H"));
    }

    public function update($id)
    {
        $dato= Persona::findOrFail($id);

        $datos= Request::all();

        \DB::table('personas')->where('id', '=', $id)->update([
                'nombre'      => $datos["nombre"],
                'documento' => $datos["documento"],
                'fecha_nac'  => $datos["fecha_nac"],
                'sexo'  => $datos["sexo"],
                'municipio'      => $datos["municipios"],
                'direccion' => $datos["direccion"],
                'telefono'    => $datos["telefono"],
            ]

        );

        \DB::table('historias')->where('persona_id', '=', $dato->id)->update([
                'diagnostico'  => $datos["diagnostico"],
                'medico_id'    => $datos["medicos"],
            ]
        );

        $id = Persona::where('id', $id)->first();

        return \View::make("general/person/partials/new_ingre", compact("id"))->with('mensaje','El usuario ha sido editado');

    }
    /*+++++++++++++++++++++++++++++BUSCAR PERSON++++++++++++++++++++++++*/
    public function get_search()
    {

        if (Request::has('parametro')) {
            $req = Request::input('parametro');
            $request = trim($req);
        } else {
            return redirect()->back();
        }

        $datos = Persona::where('documento', 'LIKE',  '%'.$request.'%')
            ->orwhere('nombre', 'LIKE',  '%'.$request.'%')
            ->orderBy('documento')
            ->groupBy('id')
            ->get();
        $result = count($datos);
        if ($result != 0) {
            return view('general/person/search', compact("datos"));
        } else {
            return redirect('person/create');
        }
    }
    public function post_regis($id=null)
    {
        $date=date('Y/m/d');
        $date2 = strtotime('+30 day', strtotime($date));
        $date2 = date('Y/m/d', $date2);

        $person= Persona::find($id);
        $retiro= Retiro::where('person_id', $id)->get();

        return view('general/caja/caja', compact("person", "retiro", "pi", "date", "date2"));
    }

    /*+++++++++++++++++++++++++++++REGISTRAR NUEVA PERSONA++++++++++++++++++++++++*/
    public function create()
    {
        $medic_option = \DB::table('medicos')->orderBy('nombrem', 'desc')->lists('nombrem','id');
        $state_option = \DB::table('estados')->orderBy('id', 'asc')->lists('nombre','id');
        $muni_option = \DB::table('municipios')->orderBy('id', 'asc')->lists('nombre','id','id_estado');
        return view('general.person.create', array ('medic_option' => $medic_option,'state_option' => $state_option, 'muni_option' => $muni_option));
    }

    public function store(RegisterUserRequest $request)
    {

        $validator = Validator::make($request->all(), $request->rules(), $request->messages());
        If ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {

            $campos = Request::all();
            $paciente = new Persona;
            $paciente->nombre = $campos["nombre"];
            if (isset($campos['group1'])){
                $paciente->documento = $campos["documento"].'-'.$campos['group1'];
            }else{
                $paciente->documento = $campos["documento"];
            }
            $fecha_nac = new Carbon($campos["fecha_nac"]);
            $paciente->fecha_nac = $fecha_nac->format('Y-m-d');
            $paciente->sexo = $campos["sexo"];
            $paciente->municipio = $campos["municipios"];
            $paciente->direccion = $campos["direccion"];
            $paciente->telefono = $campos["telefono"];
            $paciente->save();

            $id = Persona::where('documento', $paciente->documento)->first();
            $historia = new Historia();
            $historia->persona_id = $id->id;
            $historia->diagnostico = $campos["diagnostico"];
            $historia->medico_id = $campos["medicos"];
            $historia->save();

            $person_id = $id->id;
            return Redirect::route('person/caja/first', compact("person_id"));

        }
    }

    //++++++++++++++++++++TOTAL EXISTENCI++++++++++++++++++++//
    public function getExist(){

        $exist = Warehouse::where('total','>', '2')->orderBy('created_at', 'DESC')->groupBy('id_m')->get();
        dd($exist);
    }

    public function show($id){
        //
    }

    public function getCaj(){
        $id= Input::get('person_id');
        $datos=Persona::find($id);
        $history =  Entrega::where('persona_id', '=', $datos->id)->paginate(8);
        $medico_id = Historia::where('persona_id', '=', $datos->id)->first();
        $medico = Medico::where('id', '=', $medico_id->id)->first();
        // $control = Control::where('persona_id', '=', $datos->id)->orderBy('fecha', 'asc')->get();

        $pip = \DB::table('entregas')
            ->join('personas', 'personas.id','=', 'entregas.persona_id')
            ->join('medicamentos', 'medicamentos.id', '=', 'entregas.medicamento_id')
            ->join('historias', 'historias.persona_id', '=', 'entregas.persona_id')
            ->join('medicos', 'medicos.id', '=', 'historias.medico_id')
            ->select('entregas.cantidad', 'entregas.persona_id', 'entregas.medicamento_id', 'entregas.fecha', 'entregas.prox_fecha',
                'medicamentos.nombre_c','medicamentos.principio_a', 'medicamentos.composicion', 'personas.*', 'historias.medico_id',
                'medicos.nombrem')
            ->where('entregas.persona_id', '=',$id)
            ->orderBy('entregas.fecha', 'DESC')
            ->paginate(10);
        $pi2 = \DB::table('entregas')

            ->join('medicamentos', 'medicamentos.id', '=', 'entregas.medicamento_id')
            ->join('historias', 'historias.persona_id', '=', 'entregas.persona_id')
            ->select('entregas.cantidad', 'entregas.persona_id', 'entregas.medicamento_id', 'entregas.fecha',
                'medicamentos.nombre_c','medicamentos.principio_a', 'medicamentos.composicion', 'historias.medico_id')
            ->where('entregas.persona_id', '=',$id)
            ->get(15);

        return \View::make('general/person/detail', compact("datos", "history", "medico", "pip"));
    }



}