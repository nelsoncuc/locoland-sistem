<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterMedicineRequest;
use App\Medicament;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as Recu;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Validator;

class MedicineController extends Controller
{
    public function create(){
        return view('general/medicine/create');
    }

    public function store(RegisterMedicineRequest $request){
        $validator = Validator::make($request->all(), $request->rules(), $request->messages());
        If ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        } else {
            $campos = Recu::all();

            $medicine= new Medicament();
            $medicine->codigo=$campos["codigo"];
            $medicine->nombre_c=$campos["nombre_c"];
            $medicine->principio_a=$campos["principio_a"];
            $medicine->presentacion=$campos["presentacion"];
            $medicine->farmaceutica=$campos["farmaceutica"];
            $medicine->cantidad=$campos["cantidad"];
            $medicine->composicion=$campos["composicion"];
            $medicine->info=$campos["info"];
            $medicine->save();

            $datos = Medicament::where('codigo', $campos["codigo"])->first();

            //return \View::make('general/medicine/medicine_info', compact("datos"));
            return redirect("warehouse/medicine/total")->withErrors(['Registro ingresado con exito.']);


        }

    }
    
}
