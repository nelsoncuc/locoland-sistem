<?php

namespace App\Http\Controllers;
use App\Branchoffice;
use App\Purchases;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;


class ExportController extends Controller
{
    public function getPersonList($date_in, $next_in){

        $result = DB::table('entregas')
            ->join('personas', 'personas.id', '=', 'entregas.persona_id')
            ->join('medicamentos', 'medicamentos.id', '=', 'entregas.medicamento_id')
            ->select('personas.*', 'personas.documento','medicamentos.nombre_c','medicamentos.principio_a' ,'medicamentos.composicion', 'entregas.cantidad', 'entregas.fecha')
            ->where('entregas.fecha', '>', $date_in)
            ->where('entregas.fecha', '<', $next_in)
            ->groupBy('personas.id')
            ->get();
        $desde = substr($date_in,0,10);
        $hasta = substr($next_in,0,10);
        Excel::create('Medicamentos por pacientes '.$desde.' Hasta '.$hasta, function($excel) use($result){
            $excel->sheet('Entregas', function($sheet) use($result){
                $sheet->loadView('general.report.views.person_list', array('result' => $result));
            })->export('xls');
        });
    }

    public function getMList($date_in, $next_in){

        $result =  DB::table('entregas')
            ->join('medicamentos', 'medicamentos.id', '=', 'entregas.medicamento_id')
            ->select('medicamentos.nombre_c', 'medicamentos.principio_a' ,'medicamentos.composicion', 'entregas.cantidad', DB::raw('sum(entregas.cantidad) as total'))
            ->where('entregas.fecha', '>=', $date_in)
            ->where('entregas.fecha', '<=', $next_in)
            ->groupBy('medicamentos.id')
            ->get();
        $desde = substr($date_in,0,10);
        $hasta = substr($next_in,0,10);
        Excel::create('Medicamentos desde'.$desde.' hasta '.$hasta, function($excel) use($result){
            $excel->sheet('Medicamentos', function($sheet) use($result){
                $sheet->loadView('general.report.views.medi_list', array('result' => $result));
            })->export('xls');
        });
    }

    public function getPDList($date_in, $next_in){

        $result =  DB::table('entregas')
            ->distinct()
            ->join('historias', 'historias.persona_id', '=', 'entregas.persona_id')
            ->join('personas', 'personas.id', '=', 'entregas.persona_id')
            ->join('medicamentos', 'medicamentos.id', '=', 'entregas.medicamento_id')
            ->select('entregas.medicamento_id', 'personas.nombre', 'personas.direccion', 'personas.telefono', 'personas.documento', 'medicamentos.nombre_c','medicamentos.principio_a' , 'medicamentos.composicion','medicamentos.presentacion', 'personas.id', 'historias.diagnostico')
            ->where('entregas.fecha', '>=', $date_in)
            ->where('entregas.fecha', '<=', $next_in)

            ->get();
        $entregas = [];
        foreach ($result as $key => $person) {

            if (array_key_exists($person->id, $entregas)) {

                $entregas[$person->id]["medicamentos"] .= ' '.$person->nombre_c . ' ' . $person->composicion.' ('.$person->presentacion.') / ';


            } else {
                $entregas[$person->id]["id"] = $person->id;
                $entregas[$person->id]["nombre"] = $person->nombre;
                $entregas[$person->id]["documento"] = $person->documento;
                $entregas[$person->id]["direccion"] = $person->direccion;
                $entregas[$person->id]["telefono"] = $person->telefono;
                $entregas[$person->id]["diagnostico"] = $person->diagnostico;
                $entregas[$person->id]["medicamentos"] = $person->nombre_c .' '. $person->composicion .' - ('. $person->presentacion.')' ;
            }
            '<br>';

        }
        $desde = substr($date_in,0,10);
        $hasta = substr($next_in,0,10);
        Excel::create('Reporte desde'.$desde.' hasta '.$hasta, function($excel) use($entregas){
            $excel->sheet('Diagnosticos', function($sheet) use($entregas){
                $sheet->loadView('general.report.views.person_diagnostic', array('entregas' => $entregas));
            })->export('xls');
        });
    }

    public function getPMList($id){

        $result = DB::table('entregas')
            ->join('personas', 'personas.id', '=', 'entregas.persona_id')
            ->join('medicamentos', 'medicamentos.id', '=', 'entregas.medicamento_id')
            ->select('entregas.medicamento_id', 'medicamentos.nombre_c', 'medicamentos.composicion', 'personas.nombre', 'personas.documento')
            ->where('medicamentos.id', '=', $id)
            ->get();

        Excel::create('Pacientes por Medicamento', function($excel) use($result){
            $excel->sheet('Medicamentos', function($sheet) use($result){
                $sheet->loadView('general.report.views.paciente_x_medicamentos', array('result' => $result));
            })->export('xls');
        });
    }

    public function getPuExport($id){

        $purchases = \DB::table('purchases')
            ->join('medicamentos', 'medicamentos.id','=', 'id_m')
            ->select('medicamentos.codigo', 'medicamentos.id', 'medicamentos.nombre_c','purchases.id_m', 'purchases.pu', 'purchases.total','purchases.cantidad','purchases.expires', 'purchases.control', 'purchases.created_at')
            ->where('purchases.control', '=',$id)
            ->where('status','=','branch_ready')
            ->get();

        $branch_id= Purchases::where('control', $id)
            ->first();
        $branch = Branchoffice::where('id', $branch_id->branch_id)->first();
        $collection = collect($purchases);
        $sum = $collection->sum('total');

        Excel::create('Detalle recibo', function($excel) use($purchases, $branch_id, $branch, $sum){
            $excel->sheet('Recibo', function($sheet) use($purchases, $branch_id, $branch, $sum){
                $sheet->loadView('general.branchOffice.purchases_x_branch_export', array('purchases' => $purchases, 'branch_id'=> $branch_id, 'branch'=>$branch, 'sum'=>$sum));
            })->download('xls');
        });

    }

    public function getMListExt($date_in, $next_in){

        $result =  DB::table('entregas')
            ->join('medicamentos', 'medicamentos.id', '=', 'entregas.medicamento_id')
            ->select('medicamentos.nombre_c', 'medicamentos.principio_a' ,'medicamentos.composicion', 'entregas.cantidad', DB::raw('sum(entregas.cantidad) as total'))
            ->where('entregas.fecha', '>=', $date_in)
            ->where('entregas.fecha', '<=', $next_in)
            ->groupBy('medicamentos.id')
            ->get();
        $desde = substr($date_in,0,10);
        $hasta = substr($next_in,0,10);
        Excel::create('Medicamentos desde'.$desde.' hasta '.$hasta, function($excel) use($result){
            $excel->sheet('Medicamentos', function($sheet) use($result){
                $sheet->loadView('general.report.views.medi_list', array('result' => $result));
            })->export('xls');
        });
        return view('/');
    }

    public function getPersonListExt($date_in, $next_in){

        $result = DB::table('entregas')
            ->join('personas', 'personas.id', '=', 'entregas.persona_id')
            ->join('medicamentos', 'medicamentos.id', '=', 'entregas.medicamento_id')
            ->select('personas.*', 'personas.documento','medicamentos.nombre_c','medicamentos.principio_a' ,'medicamentos.composicion', 'entregas.cantidad', 'entregas.fecha')
            ->where('entregas.fecha', '>', $date_in)
            ->where('entregas.fecha', '<', $next_in)
            ->groupBy('personas.id')
            ->get();
        $desde = substr($date_in,0,10);
        $hasta = substr($next_in,0,10);
        Excel::create('Medicamentos por pacientes '.$desde.' Hasta '.$hasta, function($excel) use($result){
            $excel->sheet('Entregas', function($sheet) use($result){
                $sheet->loadView('general.report.views.person_list', array('result' => $result));
            })->export('xls');
        });
        return view('/');
    }
}
