<?php

namespace App\Http\Controllers;

use App\Entrega;
use Illuminate\Support\Facades\Request;

use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;


class ReportController extends Controller
{
    /**
     * @return mixed
     */
    public function getHistory()
    {

        return \View::make('general/report/entrega_paciente');
    }

    public function getViewMedicPaciente()
    {
        return \View::make('general/report/entrega_medicamento_paciente');

    }

    public function getEntregas()
    {

        if (Request::has('date_in')) {
            $campos = Request::all();
        } else {
            return redirect()->back();
        }


        $date = date('Y-m-d');
        $d_in = Input::get('date_in');
        $date_in = new Carbon($d_in);
        $n_in = Input::get('date_out');
        $next_in = new Carbon($n_in);

        $entregas = DB::table('entregas')
            ->distinct()
            ->join('personas', 'personas.id', '=', 'entregas.persona_id')
            ->join('medicamentos', 'medicamentos.id', '=', 'entregas.medicamento_id')
            ->select('personas.*', 'personas.documento', 'medicamentos.nombre_c', 'medicamentos.composicion', 'entregas.cantidad', 'entregas.fecha')
            ->where('entregas.fecha', '>=', $date_in)
            ->where('entregas.fecha', '<=', $next_in)
            ->groupBy('personas.id')
            ->get();

        return view('general/report/resul_report_person', compact("entregas", "date_in", "next_in"));

    }

    /**
     *
     */
    public function getMedicamentosPaciente()
    {
        if (Request::has('date_in')) {
            $campos = Request::all();
        } else {
            return redirect()->back();
        }


        $date = date('Y-m-d');
        $d_in = Input::get('date_in');
        $date_in = new Carbon($d_in);
        $n_in = Input::get('date_out');
        $next_in = new Carbon($n_in);


        $consulta = DB::table('entregas')
            ->distinct()
            ->join('historias', 'historias.persona_id', '=', 'entregas.persona_id')
            ->join('personas', 'personas.id', '=', 'entregas.persona_id')
            ->join('medicamentos', 'medicamentos.id', '=', 'entregas.medicamento_id')
            ->select('entregas.medicamento_id', 'personas.nombre', 'personas.direccion', 'personas.telefono', 'personas.documento', 'medicamentos.nombre_c', 'medicamentos.composicion','medicamentos.presentacion', 'personas.id', 'historias.diagnostico')
            ->where('entregas.fecha', '>=', $date_in)
            ->where('entregas.fecha', '<=', $next_in)

            ->get();


        $entregas = [];

        foreach ($consulta as $key => $person) {

            if (array_key_exists($person->id, $entregas)) {

                $entregas[$person->id]["medicamentos"] .= ' '.$person->nombre_c . ' ' . $person->composicion.' ('.$person->presentacion.') / ';


            } else {
                $entregas[$person->id]["id"] = $person->id;
                $entregas[$person->id]["nombre"] = $person->nombre;
                $entregas[$person->id]["documento"] = $person->documento;
                $entregas[$person->id]["direccion"] = $person->direccion;
                $entregas[$person->id]["telefono"] = $person->telefono;
                $entregas[$person->id]["diagnostico"] = $person->diagnostico;
                $entregas[$person->id]["medicamentos"] = $person->nombre_c .' '. $person->composicion .' - ('. $person->presentacion.')' ;
            }
            '<br>';

        }


        return view('general/report/resul_report_medi_person', compact("entregas", "date_in", "next_in"));


    }

    public function getMediDate()
    {

        if (Request::has('date_in')) {
            $campos = Request::all();
        } else {
            return redirect()->back();
        }


        $d_in = Input::get('date_in');
        $date_in = new Carbon($d_in);
        $n_in = Input::get('date_out');
        $next_in = new Carbon($n_in);

        $entregas = DB::table('entregas')
            ->join('medicamentos', 'medicamentos.id', '=', 'entregas.medicamento_id')
            ->select('medicamentos.nombre_c', 'medicamentos.principio_a as principio', 'medicamentos.composicion', 'medicamentos.id as id_m' ,'entregas.cantidad', DB::raw('sum(entregas.cantidad) as total'))
            ->where('entregas.fecha', '>=', $date_in)
            ->where('entregas.fecha', '<=', $next_in)
            ->groupBy('medicamentos.id')
            ->get();

        return view('general/report/resul_report_medi', compact("entregas", "date_in", "next_in"));
    }

    public function getMediHistory()
    {

        return \View::make('general/report/entrega_medi');
    }

    public function getPersonMedi($id)
    {
        $medicine = DB::table('entregas')
            ->join('personas', 'personas.id', '=', 'entregas.persona_id')
            ->join('medicamentos', 'medicamentos.id', '=', 'entregas.medicamento_id')
            ->select('entregas.medicamento_id', 'medicamentos.nombre_c', 'medicamentos.principio_a' ,'medicamentos.composicion', 'personas.nombre', 'personas.documento')
            ->where('medicamentos.id', '=', $id)
            ->get();
        return view('general/report/medi_person_result', compact("medicine", "id"));
    }
}
