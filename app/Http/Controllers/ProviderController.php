<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterProviderRequest;
use App\Provider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as Recu;

use App\Http\Requests;

class ProviderController extends Controller
{
    public function create(){
        
        
        return view('general/provider/create');
    }

    public function store(RegisterProviderRequest $request){
        
            $campos = Recu::all();

            $provider= new Provider();
            $provider->name=$campos["provider"];
            $provider->rif=$campos["rif"];
            $provider->save();

            //return \View::make('general/medicine/medicine_info', compact("datos"));
            return redirect("/warehouse/provider/create")->withErrors(['Registro ingresado con exito.']);


        }
    
}
