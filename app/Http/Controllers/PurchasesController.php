<?php

namespace App\Http\Controllers;

use App\Http\Requests\Purchases_iniRequest;

use App\Http\Requests\RegisterPurchaseRequest;
use App\Medicament;
use App\Provider;
use App\Purchases;
use App\Warehouse;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Validator;
use Illuminate\Support\Facades\Request as Recu;
use Illuminate\Http\RedirectResponse;

class PurchasesController extends Controller
{

    public function index(){
  /*      $purchase = Purchases::where('status', 'ready')
        ->groupBy('control')
            ->orderBy('id', 'DESC')
                ->get();
*/

        $purchase = \DB::table('purchases')
            ->join('provider', 'provider.id','=', 'purchases.provider_id')
            ->select('purchases.control as control', 'purchases.created_at as date', 'provider.name as name_provider')
            ->where('purchases.status', 'ready')
            ->groupBy('purchases.control')
            ->get();
        $url =URL::to('/');
        return view('general/purchases/index', compact("purchase", "url"));
    }



    public function create_ini(){
        $provider =  Provider::lists('name', 'id');
        return view('general/purchases/create_ini', compact("provider"));
    }

    public function create(Purchases_iniRequest $request){

        Session::put('codigo', Input::get('codigo'));
        Session::put('provider', Input::get('provider'));
        //$purchase2 = Purchases::where('control', Input::get('codigo'))->get();
        $purchase2 = \DB::table('purchases')
            ->join('medicamentos', 'medicamentos.id','=', 'id_m')
            ->select('medicamentos.nombre_c as nombre', 'purchases.id as idp', 'purchases.control', 'purchases.cantidad', 'purchases.expires', 'purchases.pu',
                'purchases.total')
            ->where('purchases.control', '=',Input::get('codigo'))
            ->where('purchases.status', '=','pending')
            ->get();


        return view('general/purchases/create_add', compact("purchase2"));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param \App\Http\Requests\RegisterPurchaseRequest $request
     * @return array
     */
    public function getContinue(RegisterPurchaseRequest $request){
        $codigo= $request->id_m;
        $medical = Medicament::where('codigo', $codigo)->first();

        if (empty($medical)){

            return Redirect::back()->with('status', 'No hay existencia de este medicamento en almacen!');
        }else {
            $campos = Recu::all();
            $purchase = new Purchases();
            $purchase->control = $campos["codigo"];
            $purchase->id_m = $medical->id;
            $purchase->cantidad = $campos["cantidad"];
            $purchase->expires = $campos["date_out"];
            $purchase->provider_id = $campos["provider"];
            $purchase->pu = $campos["pu"];
            $purchase->total = $campos["total"];
            $purchase->status = 'pending';
            $purchase->save();

            $purchase2 = \DB::table('purchases')
                ->join('medicamentos', 'medicamentos.id','=', 'id_m')
                ->select('medicamentos.nombre_c as nombre', 'purchases.id as idp', 'purchases.control', 'purchases.cantidad', 'purchases.expires', 'purchases.pu',
                    'purchases.total')
                ->where('purchases.control', '=',Session::get('codigo'))
                ->where('purchases.status', '=','pending')
                ->get();
            return view('general/purchases/create_add', compact("purchase2"));

        }
    }

    public function delete($id){

        \DB::table('purchases')->where('id', '=', $id)->delete();

        $purchase2 = \DB::table('purchases')
            ->join('medicamentos', 'medicamentos.id','=', 'id_m')
            ->select('medicamentos.nombre_c as nombre', 'purchases.id as idp', 'purchases.control', 'purchases.cantidad', 'purchases.expires', 'purchases.pu',
                'purchases.total')
            ->where('purchases.control', '=',Session::get('codigo'))
            ->where('purchases.status', '=','pending')
            ->get();


        return view('general/purchases/create_add', compact("purchase2"));
    }

    public function getFinal(){

        $control =  Session::get('codigo');
        $date=date('Y-m-d');
        $purchases = Purchases::where('control', $control)->get();

        foreach ($purchases  as $test){
            $almacen = Warehouse::where('id_m',$test->id_m)->orderBy('id', 'DESC')->first();
           if (empty($almacen)){
               $warehouse = new Warehouse();
               $warehouse->id_m = $test->id_m;
               $warehouse->entrega_id = $test->control;
               $warehouse->in_w = $test->cantidad;
               $warehouse->out_w ='0';
               $warehouse->total = $test->cantidad;
               $warehouse->date = $date;
               $warehouse->save();
           }else{
               $total = $almacen->total + $test->cantidad;
               $warehouse = new Warehouse();
               $warehouse->id_m = $test->id_m;
               $warehouse->entrega_id = $test->control;
               $warehouse->in_w =$test->cantidad;
               $warehouse->out_w ='0';
               $warehouse->total = $total;
               $warehouse->date = $date;
               $warehouse->save();
           }
        }
        foreach ($purchases  as $test){
            \DB::table('purchases')->where('control', '=', $test->control)->update([
                'status' => 'ready',
            ]);
        }

        return Redirect::route('puchases/shop/index');
    }

    public function getPurchasesPdf($id){

        $purchases = \DB::table('purchases')
            ->join('medicamentos', 'medicamentos.id','=', 'id_m')
            ->select('medicamentos.codigo', 'medicamentos.id', 'medicamentos.nombre_c','purchases.id_m', 'purchases.pu', 'purchases.total','purchases.cantidad','purchases.expires')
            ->where('purchases.control', '=',$id)
            ->where('status','=','ready')
            ->get();

        $branch_id= Purchases::where('control', $id)
            ->first(); /*buscando provider id... lo dejo asi para usar la misma plantilla de sucursal*/

        $branch = Provider::where('id', $branch_id->provider_id)->first();
        $collection = collect($purchases);
        $sum = $collection->sum('total');
        return view('general/report/views/pdfpurchases',compact("purchases", "branch", "branch_id", "sum"));
    }

}
