<?php

namespace App\Http\Controllers;
use App\Medicament;
use App\Provider;
use App\Warehouse;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class WarehouseController extends Controller
{
    public function create(){

        $provider = Provider::lists('name', 'rif');
        
        return view('general/warehouse/create', compact("provider"));
    }

    //++++++++++++++++++++TOTAL EXISTENCI++++++++++++++++++++//
    public function getExist(){

        $exist   =  DB::select(DB::raw('SELECT * FROM (SELECT DISTINCT * FROM warehouse ORDER BY id DESC )sub INNER JOIN medicamentos ON id_m = medicamentos.id GROUP BY id_m'));

        return view('general/warehouse/exist', compact("exist"));
    }
    
    public function getMedicine(){
        $medicine = Medicament::orderBy('id', 'DESC')->get();
        return view('general/warehouse/medicine', compact("medicine"));
    }
    
    //++++++++++++++++++agregar Medicamentos++++++++++++++++++++++++//
    
    
}
