<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterPurchaseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_m'     => 'required',
            'cantidad'     => 'required|numeric',
            'date_out'     => 'required',
            'pu'     => 'required|numeric',
            'total'     => 'required|numeric',

        ];
    }
    public function messages(){
        return [

            'id_m.required' => 'El campo es requerido',
            'cantidad.required' => 'El campo es requerido',
            'cantidad.nuemric' => 'El campo solo debe contener valores numericos',
            'date_out.required' => 'El campo es requerido',
            'pu.required' => 'El campo es requerido',
            'pu.numeric' => 'Use punto para los decimales y use solo valores numericos',
            'total.required' => 'El campo es requerido',
            'total.numeric' => 'Use punto para los decimales y use solo valores numericos',



        ];
    }
}
