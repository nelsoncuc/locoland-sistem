<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:3|max:50',
            'documento' => 'required|unique:personas,documento',
            'fecha_nac' => 'required|date',
            'sexo'      => 'required',

            'direccion' => 'required',
            'diagnostico'=> 'required'

        ];
    }

    public function messages(){
        return [

            'nombre.required' => 'El campo es requerido',
            'nnombre.min' => 'El mínimo de caracteres permitidos son 3',
            'nombre.max' => 'El máximo de caracteres permitidos son 16',
            'documento.required' => 'El campo es requerido',
            'documento.unique' => 'El numero de documento ya se encuantra registrado',
            'fecha_nac.required' => 'debe ingresar fecha nacimiento',
            'sexo.required' => 'Indique el sexo del paciente',

            'direccion.required' => 'El campo es requerido',
            'diagnostico.required' => 'El campo es Obligatorio'

        ];
    }
}
