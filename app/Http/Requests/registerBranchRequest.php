<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class registerBranchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cantidad' => 'required',
            'id_m' => 'required|exists:medicamentos,codigo'
        ];
    }

    public function messages(){
        return [

            'cantidad.required' => 'El campo Cantidad es requerido',
            'id_m.required' => 'El campo Codigo es requerido',
            'id_m.exists' => 'Este medicamento no se encuentra registrado'

        ];
    }
}
