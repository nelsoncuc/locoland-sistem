<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterMedicineRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigo'       => 'required|unique:medicamentos,codigo',
            'nombre_c'     => 'required',
            'principio_a'  => 'required',
            'farmaceutica' => 'required',
            'cantidad'     => 'required|integer',
            'composicion'  => 'required',

        ];
    }
    public function messages(){
        return [

            'codigo.required' => 'El campo es requerido',
            'codigo.unique' => 'Este codigo ya se encuentra rgistrado',
            'nombre_c.required' => 'El campo es requerido',
            'principio_a.required' => 'El campo es requerido',
            'farmaceutica.required' => 'El campo es requerido',
            'cantidad.required' => 'El campo es requerido',
            'cantidad.integer' => 'El campo debe contener un valor numerico',
            'composicion.required' => 'El campo es requerido'

        ];
    }
}
