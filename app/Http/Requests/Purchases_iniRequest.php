<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class Purchases_iniRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'codigo'     => 'required|numeric|unique:purchases,control',
            'provider'     => 'required',
            
        ];
    }
    public function messages(){
        return [

            'provider.required' => 'El campo es requerido',
            'codigo.numeric' => 'El campo solo debe contener valores numericos',
            'codigo.required' => 'El campo es requerido',
            'codigo.unique' => 'Este Nro de referencia ya ha sido registrado',

        ];
    }
}
