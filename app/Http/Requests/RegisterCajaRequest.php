<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterCajaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cantidad' => 'required',
            'codigo' => 'required|exists:medicamentos,codigo'
        ];
    }

    public function messages(){
        return [

            'cantidad.required' => 'El campo es requerido',
            'codigo.required' => 'El mínimo de caracteres permitidos son 3',
            'codigo.exists' => 'Este medicamento no se encuentra registrado'

        ];
    }
}
