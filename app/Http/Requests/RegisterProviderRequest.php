<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class RegisterProviderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'provider'     => 'required',
            'rif'     => 'required|unique:provider,rif',

        ];
    }
    public function messages(){
        return [

            'provider.required' => 'El campo es requerido',
            'rif.unique' => 'Este codigo ya se encuentra rgistrado',
            'rif.required' => 'El campo es requerido',

        ];
    }
}
