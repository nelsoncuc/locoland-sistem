<?php


/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

include __DIR__ . '/routes/login_user.php';

Route::group(['middleware' => ['auth']], function(){
include __DIR__ . '/routes/warehouse.php';
include __DIR__ . '/routes/medicine.php';
include __DIR__ . '/routes/provider.php';
include __DIR__ . '/routes/branchoffice.php';
include __DIR__ . '/routes/caja.php';
include __DIR__ . '/routes/person.php';
include __DIR__ . '/routes/export.php';
include __DIR__ . '/routes/purchases.php';

});
include __DIR__ . '/routes/nologin.php';

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

//Route::auth();

//Route::get('/home', 'HomeController@index');












