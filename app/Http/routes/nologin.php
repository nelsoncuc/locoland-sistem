<?php
Route::get('export/getpuexport/{id}',
    ['as' => 'export/getpuexport/{id}',
        'uses' => 'ExportController@getPuExport']);

Route::get('export/getReport30/{date_in}/2/{date}',
    ['as' => 'export/getReport30/{date_in}/2/{date}',
        'uses' => 'ExportController@getMListExt']);

Route::get('export/getReport30patient/{date_in}/2/{date}',
    ['as' => 'export/getReport30patient/{date_in}/2/{date}',
        'uses' => 'ExportController@getPersonListExt']);