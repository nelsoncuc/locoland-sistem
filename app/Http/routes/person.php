<?php

Route::group(['middleware' => ['web']], function(){

    Route::resource('person', 'DashController');
    Route::resource('paciente', 'DashController');
    Route::get('person/detail/{id}', 'DashController@get_detail');

});

Route::post('person/search', [
    'as' => 'person/search',
    'uses' => 'DashController@get_search'
]);