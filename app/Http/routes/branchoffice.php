<?php
/*
 * rutas para las sucursales
 */

Route::get('branchoffice/branchoffice',
    ['as' => 'branchoffice/branchoffice',
        'uses' => 'branchOfficeController@index']);


Route::group(['middleware' => ['web']], function () {
    Route::get('branchoffice/create',
    ['as' => 'branchoffice/create',
        'uses' => 'branchOfficeController@create']);

Route::post('branchoffice/store',
    ['as' => 'branchoffice/store',
        'uses' => 'branchOfficeController@store']);

Route::get('branchoffice/edit/{id}',
    ['as' => 'branchoffice/edit/{id}',
        'uses' => 'branchOfficeController@edit']);

    Route::put('branchoffice/{id}/update', [
        'as'   => 'branchoffice.update',
        'uses' => 'branchOfficeController@update']);

});

Route::get('branchoffice/register/ini/{id}/{code}',
    ['as' => 'branchoffice/register/ini/{id}/{code}',
        'uses' => 'branchOfficeController@register_ini']);

Route::group(['middleware' => ['web']], function () {

    Route::post('branchoffice/register/register_med',
        ['as' => 'branchoffice/register/register_med',
            'uses' => 'branchOfficeController@register_med']);

});
Route::get('branchoffice/register/delete/{id}',
    ['as' => 'branchoffice/register/delete/{id}',
        'uses' => 'branchOfficeController@delete']);

Route::get('branchoffice/register/add/{id}',
    ['as' => 'branchoffice/register/add/{id}',
        'uses' => 'branchOfficeController@getFinal']);

Route::get('branchoffice/purchases/view/{id}',
    ['as' => 'branchoffice/purchases/view/{id}',
        'uses' => 'branchOfficeController@getPurchases']);

Route::get('branchoffice/getPurchasesPdf/{id}',
    ['as' => 'branchoffice/getPurchasesPdf/{id}',
        'uses' => 'branchOfficeController@getPurchasesPdf']);

Route::get('branchoffice/listPurchases/{id}',
    ['as' => 'branchoffice/listPurchases/{id}',
        'uses' => 'branchOfficeController@getListPurchases']);

Route::get('branchoffice/delete/{id}',
    ['as' => 'branchoffice/delete/{id}',
        'uses' => 'branchOfficeController@deleteBranch']);