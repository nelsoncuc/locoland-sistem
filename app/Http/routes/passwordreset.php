<?php
/**
 * Created by PhpStorm.
 * User: algoritmhus
 * Date: 2/06/16
 * Time: 11:24
 */

// Password reset link request routes------------------------------
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');
//-----------------------------------------------------------------

// Password reset routes-------------------------------------------------
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');
//------------------------------------------------------------------------