<?php
//++++++++++++++++ingresar facturasde compra****************//

Route::group(['middleware' => ['web']], function () {
    //Route::resource('/puchases/shop',  'PurchasesController');

    Route::get('puchases/shop/index', [
        'as' => 'puchases/shop/index',
        'uses' => 'PurchasesController@index'
    ]);

    Route::get('puchases/shop/create_ini', [
        'as' => 'puchases/shop/create_ini',
        'uses' => 'PurchasesController@create_ini'
    ]);

    Route::get('puchases/shop/create', [
        'as' => 'puchases/shop/create',
        'uses' => 'PurchasesController@create'
    ]);

    Route::post('puchases/shop/continue', [
        'as' => 'puchases/shop/continue',
        'uses' => 'PurchasesController@getContinue'
    ]);

    Route::get('puchases/shop/delete/{id}',
        ['as' => 'puchases/shop/delete/{id}',
            'uses' => 'PurchasesController@delete']);

    Route::get('puchases/shop/add/{id}', [
        'as' => 'puchases/shop/add/{id}',
        'uses' => 'PurchasesController@getFinal'
    ]);

    Route::get('purchases/getPurchasesPdf/{id}',
        ['as' => 'purchases/getPurchasesPdf/{id}',
            'uses' => 'PurchasesController@getPurchasesPdf']);

});
