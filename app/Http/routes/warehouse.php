<?php


//++++++++++++++++existencia almacen****************//

Route::get('warehouse/exist', [
    'as' => 'warehouse/exist',
    'uses' => 'WarehouseController@getExist'
]);

Route::group(['middleware' => ['web']], function () {
    Route::resource('/warehouse/shop/new',  'WarehouseController');

    Route::get('warehouse/medicine/total', [
        'as' => 'warehouse/medicine/total',
        'uses' => 'WarehouseController@getMedicine'
    ]);
});

//++++++++++++++++reporte por pacientes****************//



Route::post('report/entregas_p', [
    'as' => 'report/entregas_p',
    'uses' => 'ReportController@getEntregas'
]);

Route::post('report/entregas_m_p', [
    'as' => 'report/entregas_m_p',
    'uses' => 'ReportController@getMedicamentosPaciente'
]);



Route::get('report/history_person', [
    'as' => 'report/history_person',
    'uses' => 'ReportController@getHistory'
]);

Route::get('report/medicamento_person', [
    'as' => 'report/medicamento_person',
    'uses' => 'ReportController@getViewMedicPaciente'
]);

//++++++++++++++++reporte por medicamentos****************//

Route::post('report/entregas_m', [
    'as' => 'report/entregas_m',
    'uses' => 'ReportController@getMediDate'
]);

Route::get('report/history_medi', [
    'as' => 'report/history_medi',
    'uses' => 'ReportController@getMediHistory'
]);

Route::get('report/person_medi/{id}', [
    'as' => 'report/person_medi/{id}',
    'uses' => 'ReportController@getPersonMedi'
]);

