<?php
//**************exportar reporte personas con medicamentos entregados por rango de fechas*****************/

Route::get('export/getPersonList/{date_in}/2/{next_in}',
    ['as' => 'export/getPersonList/{date_in}/2/{next_in}',
        'uses' => 'ExportController@getPersonList']);

Route::get('export/getMList/{date_in}/2/{next_in}',
    ['as' => 'export/getMList/{date_in}/2/{next_in}',
        'uses' => 'ExportController@getMList']);

Route::get('export/getPersonsDiagnostic/{date_in}/2/{next_in}',
    ['as' => 'export/getPersonsDiagnostic/{date_in}/2/{next_in}',
        'uses' => 'ExportController@getPDList']);

Route::get('export/getPersonsMedicine/{id}',
    ['as' => 'export/getPersonsMedicine/{id}',
        'uses' => 'ExportController@getPMList']);


