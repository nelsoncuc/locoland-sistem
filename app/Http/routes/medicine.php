<?php

//++++++++++++++++medicine register****************//

Route::group(['middleware' => ['web']], function () {
    Route::resource('/warehouse/medicine',  'MedicineController');

    Route::get('/warehouse/medicine/add/create', [
        'as' => '/warehouse/medicine/add/create',
        'uses' => 'ExtraController@create'
    ]);

    Route::post('/warehouse/medicine/add/store', [
        'as' => '/warehouse/medicine/add/store',
        'uses' => 'ExtraController@store'
    ]);
});

