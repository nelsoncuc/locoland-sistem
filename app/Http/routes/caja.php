<?php
Route::group(['middleware' => ['web']], function() {

    Route::resource('caja/store', 'CajaController@store');
    Route::resource('caja/create', 'CajaController@create');
    Route::group(['prefix' => '/'], function(){
        Route::resource('caja', 'CajaController');
    });
});

//++++++++++++PASO A MODULO DE CAJA+++++++++++++++++++++++++++
Route::get('person/caja/first', [
    'as' => 'person/caja/first',
    'uses' => 'DashController@getCaj'
]);

Route::get('person/caja/{id}', 'DashController@post_regis');

