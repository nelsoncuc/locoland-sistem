<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchases extends Model
{
    protected $table = 'purchases';
    public $timestamps = true;
    protected $fillable = ['control', 'id_m', 'cantidad','expires', 'provider_id', 'pu', 'total', 'status', 'branch_id'];

}
