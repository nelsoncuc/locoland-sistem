<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model {

    protected $table = 'personas';
    public $timestamps = false;
    protected $fillable = ['nombre', 'documento', 'fecha_nac','sexo', 'municipio', 'direccion', 'telefono', 'email'];


    public function entrega()
    {
        return $this->hasMany('app\Entrega', 'id', 'persona_id');
    }

    public function control()
    {
        return $this->hasMany('app\Control', 'id', 'persona_id');
    }

    public function historia()
    {
        return $this->hasOne('app\Historia', 'persona_id', 'id');
    }

    public function getAgeAttribute()
    {
        return \Carbon\Carbon::parse($this->fecha_nac)->age;
    }


}
