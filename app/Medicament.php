<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicament extends Model
{
    protected $table = 'medicamentos';
    public $timestamps = false;
    public function pEntrega()
    {
        return $this->hasMany('App\Entrega', 'persona_id');
    }

    public function Pretiros()
    {
        return $this->hasMany('App\Retiro', 'medicamento_id');
    }

    public function Pwarehouse()
    {
        return $this->hasMany('App\Warehouse','id', 'id_m');
    }
}
