<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Historia extends Model {

	protected $table = 'historias';
    protected $fillable = ['id', 'persona_id', 'diagnostico','medico_id'];
    public function userHistoria()
    {
        return $this->belongsTo('App\Persona','persona_id','id');
    }

    public function entrega(){

        return $this->hasMany('App\Entrega', 'id', 'persona_id');

    }

    public function medico(){

        return $this->belongsTo('App\Medico', 'medico_id', 'id');

    }
    public function medicoh()
    {
        return $this->hasOne('App\Medicos', 'medico_id', 'id');
    }
}
